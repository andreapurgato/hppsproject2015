/**
 * 
 */
package it.polimi.scheduler;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Davide Tantillo
 * @author Andre Purgato
 */
public class Task {

	//The id of the task contained into the xml file
	public String idTask;
	
	//List containing all the inGoing tasks
	public List<Task> inGoing;
	
	//List containing all the outGoing tasks
	public List<Task> outGoing;
	
	//True if the task is into the critical path
	public boolean critical_path;

	//True if the task has been assigned with an HW implementation, False if it is SW
	public boolean hw;
	
	//The execution time actually used by the task
	public int execution_time;
	
	//The exact starting time where the task has been scheduled
	public int t_start;
	
	//The exact ending time where the task has been scheduled
	public int t_end;
	
	//The window where the task can be scheduled without creating delays 
	public int t_min;
	public int t_max;
	
	//The maximum delay allowed to this task without creating delay to other tasks (0 --> critical path)
	public int slack;
	
	//The resources actually requested by this task
	public int clb;
	public int dsp;
	public int bram;
	
	//The efficiency of the selected implementation of the task
	public int efficiency;
	
	/*
	 * This variable can assume three value:
	 * 		1: the task has been scheduled into cpu 1
	 * 		2: the task has been scheduled into cpu 1
	 * 		0: the task has not been scheduled into a cpu (HW)
	 */
	public int cpu;
	
	//The area where the task is mapped (if HW)
	public Area area_ref;
	
	/*
	 * Structure used to memorize all implementation information.
	 * execution_list_hw, lut_list, dsp_list, bram_list, ff_list,
	 * clb_list, efficiency_implementation_hw are aligned.
	 * It means that all the information regarding i-th hw
	 * implementation are in the i-th position. 
	 */
	public List<Integer> execution_list_hw;
	public List<Integer> lut_list; //le lut nell'xml si chiamano area
	public List<Integer> dsp_list;
	public List<Integer> bram_list;
	public List<Integer> ff_list;
	public List<Integer> clb_list;
	public List<Integer> efficiency_implementation_hw;
	public List<Integer> execution_list_sw;
	
	//Field used to understand if a task has been delayed
	public boolean updatedForDelayReconfiguration;
	
	
	//Inizialize the name and all the structure of the task
	public Task (String idTask) {
		this.idTask = idTask;
		inGoing = new ArrayList<Task>();
		outGoing = new ArrayList<Task>();
		execution_list_hw = new ArrayList<Integer>();
		execution_list_sw = new ArrayList<Integer>();
		lut_list = new ArrayList<Integer>();
		dsp_list  = new ArrayList<Integer>();
		bram_list = new ArrayList<Integer>();
		ff_list = new ArrayList<Integer>();
		clb_list = new ArrayList<Integer>();
		efficiency_implementation_hw = new ArrayList<Integer>();
		cpu = 0;
	}
	
	/*
	 * This function simply compare all the possible implementation and initialize
	 * the resource and time variable with the values of the extracted implementation
	 */
	public void extractBestImplementation (int weightClb, int weightDsp, int weightBram, int ff_in_clb, int lut_in_clb) {
		
		//Search the best HW implementation (only if there are HW implementation)
		if (!execution_list_hw.isEmpty()) {
			
			//Scan all the HW implementation
			for (int i = 0; i < execution_list_hw.size(); i++) {
				
				//Calculate the clb used because clb are the maximum between FF and LUT 
				if (ff_list.get(i)/ff_in_clb > lut_list.get(i)/lut_in_clb) {
					clb_list.add(ff_list.get(i)/ff_in_clb);
				} else {
					clb_list.add(lut_list.get(i)/lut_in_clb);
				}
				
				//Calculate the efficiency of the current implementation
				int efficiency = 
						execution_list_hw.get(i) / (weightClb*clb_list.get(i) +
						weightDsp*dsp_list.get(i) +
						weightBram*bram_list.get(i));
				
				//Inserting it in the structure that contains the efficiency of the HW's implementation
				efficiency_implementation_hw.add(efficiency);
				
			}
			
			//Extract the best HW implementation using the efficiency value
			extractBestHwImplementationRatio();
		}
		
		
		/*
		 * Compare the execution time of the most efficient HW implementation with the SW implementations.
		 * If SW is better or doesn't exist any HW implementation, assign to the implementation variables
		 * the fastest SW implementation.
		 */	
		for (int i = 0; i < execution_list_sw.size(); i++) {
			if (execution_list_sw.get(i) < execution_time || execution_time == 0) {
				execution_time = execution_list_sw.get(i);
				clb = 0;
				dsp = 0;
				bram = 0;
				efficiency = 0;
				hw = false;
			}
		}
		
	}
	
	/*
	 * This function is used to select the best SW implementation
	 * from the ones available and assign its value to the implementation
	 * variable of the task.
	 */
	public void switchToSoftwareImplementation(){
		
		//Delete all resources usage
		clb = 0;
		dsp = 0;
		bram = 0;
		efficiency = 0;
		hw = false;
		
		//Search the fastest SW implementation
		execution_time = execution_list_sw.get(0);
		
		for (int i = 0; i < execution_list_sw.size(); i++) {
			if (execution_list_sw.get(i) < execution_time) {
				execution_time = execution_list_sw.get(i);
			}
		}
		
	}
	
	
	public boolean switchToHardwareImplementation(){
		
		if(efficiency_implementation_hw.isEmpty())
			return false;
		
		//Extract the best HW implementation
		extractBestHwImplementationRatio();
		
		return true;
		
	}
	
	/*
	 * This function is used to select the best HW searching the ones
	 * with the best usage ratio.
	 * Usage ratio is the ratio between the Clock Cycles (cc) and the resource used
	 * by an implementation. So it evaluates how many cc a single resource exploits.
	 */
	private void extractBestHwImplementationRatio () {
		
		//Extract the best HW implementation using the efficiency value
		int min = efficiency_implementation_hw.get(0);
		int min_index = 0;
		
		for (int i = 0; i < efficiency_implementation_hw.size(); i++) {
			
			if (efficiency_implementation_hw.get(i) > min) {
				min = efficiency_implementation_hw.get(i);
				min_index = i;
				hw = true;
			}
			
		}
		
		//Assign to the implementation variables of the task with the ones of the best HW implementation
		execution_time = execution_list_hw.get(min_index);
		clb = clb_list.get(min_index);
		dsp = dsp_list.get(min_index);
		bram = bram_list.get(min_index);
		efficiency = efficiency_implementation_hw.get(min_index);
		hw = true;
		
	}
}
