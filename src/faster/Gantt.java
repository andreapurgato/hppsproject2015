package faster;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Gantt extends JFrame{

	public static final int X_MAX = 1900;
	public int max;
	
	public List<Task> taskToDraw;
	public List<Area> listArea;
	public Map<Area, Integer> mapArea;
	
    public Gantt(List<Task> taskToDraw, List<Area> listArea){
    	
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	this.taskToDraw = taskToDraw;
    	this.listArea = listArea;
    	mapArea = new HashMap<Area, Integer>();
    	
    	int i = 280;
    	for (Area area : listArea) {
			mapArea.put(area, i);
			i+=70;
		}
    	
    	
        JPanel panel=new JPanel();
        getContentPane().add(panel);
        setSize(X_MAX, 1000);
    }

    public float scale_x (int value) {
    	float val = value;
    	return (val / max) * (X_MAX - 200) + 100;
    }
    
    public void paint(Graphics g) {
    	Graphics2D g2 = (Graphics2D) g;
    	Font font = new Font("Serif", Font.BOLD, 16);
        g2.setFont(font);
    	
    	if (taskToDraw.size() == 0) {
    		System.out.println("NO TASK AVAILABLE");
			return;
		}
    	
    	for (Task t : taskToDraw) {
			if (t.t_max > max) {
				max = t.t_max;
			}
		}
    	
    	//Assi che non funzionano
    	Line2D line;
    	super.paint(g);
    	line = new Line2D.Double(100, 0, 100, 1000);
    	g2.draw(line);
    	line = new Line2D.Double(1800, 0, 1800, 1000);
    	g2.draw(line);
    	//
    	g2.setStroke(new BasicStroke(2));

    	int i = 0;
        for (Task t : taskToDraw) {
        	
        	int ypsoln = 0;
        	
			if (t instanceof ReconfigurationTask) {
				ypsoln = 210;
				i+=50;
				if (i>255) i=0;
			} else if (!t.hw) {
				
				if (t.cpu == 1) {
					ypsoln = 70;
				} else if (t.cpu == 2) {
					ypsoln = 140;
				}
				
				
			} else {
				ypsoln = mapArea.get(t.area_ref);
			}
			
			Rectangle2D r = new Rectangle2D.Double(scale_x(t.t_start), ypsoln, scale_x(t.t_end) - scale_x(t.t_start), 20);
			g2.draw(r);
			g2.drawString(t.idTask, (int) (((r.getX() + r.getWidth()/4))), (int) (r.getY()+r.getHeight()-4));
			
			r = new Rectangle2D.Double(scale_x(t.t_min), ypsoln+20, scale_x(t.t_max) - scale_x(t.t_min), 20);
			
			if (t instanceof ReconfigurationTask) {
				g2.setPaint(new Color(i, i, i));
			}
			
			g2.draw(r);
			g2.drawString(t.idTask, (int) (((r.getX() + r.getWidth()/4))), (int) (r.getY()+r.getHeight()-4));
			
			line = new Line2D.Double(100, ypsoln+55, 1800, ypsoln+55);
			g2.setPaint(Color.BLACK);
	    	g2.draw(line);
	    	
		}
        
        for (Area area : listArea) {
        	g2.drawString("Area " + area.id, 40, (int) mapArea.get(area) + 25);
		}
        g2.drawString("CPU 1", 40, 95);
        g2.drawString("CPU 2", 40, 165);
        g2.drawString("Reconf ", 40, 235);
        
    }

}