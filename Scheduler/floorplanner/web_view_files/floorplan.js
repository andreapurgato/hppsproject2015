var FPGAcanvas;
var regionCanvas;
var linesCanvas;
var canvasINFO = null;
var numberOfRegions = 0;
var regionsSet = [];
var isValid = false;
var maxRes = {};
var qWL = 0.0, qP = 0.0, qR = 0.0;
var regionName = {};



function setupCanvas(info, fromCache) {

	fromCache = typeof fromCache !== 'undefined' ? fromCache : 1;	

	// compute FPGA matrix
	info.matrix = [];
	for(var y = 0; y <= info.maxTY; y++)
		info.matrix[y] = [];

	for(var y = 0; y <= info.maxTY; y++)
		for(var x = 0; x <= info.maxTX; x++)
			info.matrix[y][x] = 'NULL';

	for(var p in info.portions)
		for(var y = info.portions[p].y1; y <= info.portions[p].y2; y++)
			for(var x = info.portions[p].x1; x <= info.portions[p].x2; x++)
				info.matrix[y][x] = info.portions[p].type;


	// compute resNum
	info.resNum = [];
	for(var t in info.resources)
		info.resNum[t] = info.resources[t].number;



	// compute tileWG and tileHG
	var tileWG = 9;
	var tileHG = 16 + info.resNum['CLB']*3;

	info.tileWG = tileWG;
	info.tileHG = tileHG;

	// setup canvas width and height
	var c = document.getElementById('FPGAcanvas');
	c.setAttribute('width', info['tileWG'] * info['maxTX'] + 3);
	c.setAttribute('height', info['tileHG'] * info['maxTY'] + 3);
	c = document.getElementById('regionCanvas');
	c.setAttribute('width', info['tileWG'] * info['maxTX'] + 3);
	c.setAttribute('height', info['tileHG'] * info['maxTY'] + 3);
	c = document.getElementById('LINEScanvas');
	c.setAttribute('width', info['tileWG'] * info['maxTX'] + 3);
	c.setAttribute('height', info['tileHG'] * info['maxTY'] + 3);

	// initializes the canvas
	canvasINFO = info;
	FPGAcanvas = new fabric.StaticCanvas('FPGAcanvas');
	FPGAcanvas.selection = false;
	linesCanvas = new fabric.StaticCanvas('LINEScanvas');
	linesCanvas.selection = false;
	regionCanvas = new fabric.Canvas('regionCanvas');
	regionCanvas.selection = false;


	if(!fromCache)
	{
		// compute drawing functions
		var drawF = [];
		drawF['CLB'] = function(canvas,row,col) {

			for(var j = 0; j < 2; j++) {
				for(var i = 0; i < info.resNum['CLB']; i++) {
					var x = j*3 + 1 + tileWG*col;
					var y = i*3 + 8 + tileHG*row;

					canvas.add(new fabric.Rect({
					  left: x,
					  top: y,
					  fill: 'rgba(0,0,0,0)',
					  width: 4,
					  height: 2,
					  strokeWidth:1,
					  stroke: 'rgba(64,64,255,1)',
					  hasRotatingPoint: false,
					  transparentCorners: true,
					  selectable: false
					}));
				}
			}
		};

		drawF['DSP'] = function(canvas,row,col) {
			for(var i = 0; i < info.resNum['DSP']; i++) {
				var x = 2 + tileWG*col;
				var hSpace = (tileHG - 16) / info.resNum['DSP'];
				var h = hSpace - 3;
				var y = i*((tileHG - 16 + hSpace - h) / info.resNum['DSP']) + 8 + tileHG*row;

				canvas.add(new fabric.Rect({
				  left: x,
				  top: y,
				  fill: 'rgba(64,255,64,1)',
				  width: 5,
				  height: h,
				  strokeWidth:1,
				  stroke: 'rgba(64,255,64,1)',
				  hasRotatingPoint: false,
				  transparentCorners: true,
				  selectable: false
				}));
			}
		}
		drawF['BRAM'] = function(canvas,row,col) {
			for(var i = 0; i < info.resNum['BRAM']; i++) {
				var x = 2 + tileWG*col;
				var hSpace = (tileHG - 16) / info.resNum['BRAM'];
				var h = hSpace - 3;
				var y = i*((tileHG - 16 + hSpace - h) / info.resNum['BRAM']) + 8 + tileHG*row;

				canvas.add(new fabric.Rect({
				  left: x,
				  top: y,
				  fill: 'rgba(255,64,255,1)',
				  width: 5,
				  height: h,
				  strokeWidth:1,
				  stroke: 'rgba(255,64,255,1)',
				  hasRotatingPoint: false,
				  transparentCorners: true,
				  selectable: false
				}));
			}
		}
		drawF['-F-'] = function(canvas,row,col) {
			canvas.add(new fabric.Rect({
			  left: tileWG*col,
			  top: tileHG*row,
			  fill: 'rgba(40,40,40,1)',
			  width: tileWG,
			  height: tileHG,
			  strokeWidth:0,
			  hasRotatingPoint: false,
			  transparentCorners: true,
			  selectable: false
			}));
		}
		drawF['NULL'] = function(canvas,row,col) {}

		info.drawF = drawF;

		// initialize the amount of available resources for each resource type
		for(var t in info.resources) {
			maxRes[t] = 0;
		}

		// draw the fpga matrix
		FPGAcanvas.renderOnAddRemove = false;
		for(row = 0; row < info['maxTY']; row++) {
			for(col = 0; col < info['maxTX']; col++) {
				var res = info['matrix'][info['maxTY']-row-1][col];
				func = info['drawF'][res];
				func(FPGAcanvas, row, col);
				maxRes[res] += info['resNum'][res];
			}
		}
		FPGAcanvas.renderAll();

	}
	else
	{
		// initialize the amount of available resources for each resource type
		for(var t in info.resources) {
			maxRes[t] = info.resources[t].total;
		}

		fabric.Image.fromURL('../../web_view_files/cache/' + info.name + '.png', function(img) {
		  FPGAcanvas.add(img);
		});

		FPGAcanvas.renderAll();
	}


	// handles region selection
	regionCanvas.observe("object:selected", function(e) {
		var shape = e.target;
		shape.moveTo(numberOfRegions*2 - 1);
		regionsSet[shape.number].text.moveTo(numberOfRegions*2 - 1);
	});

	// handles region moves
	regionCanvas.observe("object:moving", function(e) {
		var shape = e.target;
		var currentWidth = shape.currentWidth;
		var currentHeight = shape.currentHeight;
		var posX = Math.floor(shape.left / canvasINFO['tileWG'] + 0.5) * canvasINFO['tileWG'];
		var posY = Math.floor(shape.top / canvasINFO['tileHG'] + 0.5) * canvasINFO['tileHG'];

		if(posX < 0)
			posX = 0;
		if(posY < 0)
			posY = 0;
		if(posX + currentWidth > canvasINFO['maxTX'] * canvasINFO['tileWG'])
			posX = canvasINFO['maxTX'] * canvasINFO['tileWG'] - currentWidth + shape.strokeWidth;
		if(posY + currentHeight > canvasINFO['maxTY'] * canvasINFO['tileHG'])
			posY = canvasINFO['maxTY'] * canvasINFO['tileHG'] - currentHeight + shape.strokeWidth;


		var text = regionsSet[shape.number].text;
		text.set({left: posX});
		text.set({top: posY});
		shape.set({left: posX});
		shape.set({top: posY});

		updateCoveredRes(shape);
		updateConn();
		updateObj();
	});


	// handles regions scaling
	regionCanvas.observe("object:scaling", function(e) {
		var shape = e.target;
		var posX = Math.floor(shape.left / canvasINFO['tileWG'] + 0.5) * canvasINFO['tileWG'];
		var posY = Math.floor(shape.top / canvasINFO['tileHG'] + 0.5) * canvasINFO['tileHG'];
		var width = shape.width;
		var height = shape.height;

		var scaleX = (Math.floor(width*shape.scaleX / canvasINFO['tileWG'] + 0.5) * canvasINFO['tileWG']) / width;
		var scaleY = (Math.floor(height*shape.scaleY / canvasINFO['tileHG'] + 0.5) * canvasINFO['tileHG']) / height;

		if(shape.__corner == 'ml')
		{
			var right = Math.floor((shape.left + width*shape.scaleX ) / canvasINFO['tileWG'] + 0.5) * canvasINFO['tileWG'];
			scaleX = (right - posX) / shape.width;
		}
		if(shape.__corner == 'mt')
		{
			var bottom = Math.floor((shape.top + height*shape.scaleY ) / canvasINFO['tileHG'] + 0.5) * canvasINFO['tileHG'];
			scaleY = (bottom - posY) / shape.height;
		}

		if(scaleX == 0)
			scaleX = canvasINFO['tileWG'] / width;
		if(scaleY == 0)
			scaleY = canvasINFO['tileHG'] / height;

		var currentWidth = width*scaleX;
		var currentHeight = height*scaleY;


		if(posX < 0)
		{
			posX = 0;
			scaleX = (Math.floor((currentWidth + shape.left) / canvasINFO['tileWG'] + 0.5) * canvasINFO['tileWG']) / width;
			if(scaleX == 0)
				scaleX = canvasINFO['tileWG'] / width;
		}
		if(posY < 0)
		{
			posY = 0;
			scaleY = (Math.floor((currentHeight + shape.top) / canvasINFO['tileHG'] + 0.5) * canvasINFO['tileHG']) / height;
			if(scaleY == 0)
				scaleY = canvasINFO['tileHG'] / height;
		}
		if(posX + currentWidth > canvasINFO['maxTX'] * canvasINFO['tileWG'])
		{
			scaleX = (Math.floor((canvasINFO['maxTX']*canvasINFO['tileWG'] - posX) / canvasINFO['tileWG'] + 0.5) * canvasINFO['tileWG']) / width;
			if(scaleX == 0)
			{
				scaleX = canvasINFO['tileWG'] / width;
				posX = (canvasINFO['maxTX']-1)*canvasINFO['tileWG'];
			}
		}
		if(posY + currentHeight > canvasINFO['maxTY'] * canvasINFO['tileHG'])
		{
			scaleY = (Math.floor((canvasINFO['maxTY']*canvasINFO['tileHG'] - posY) / canvasINFO['tileHG'] + 0.5) * canvasINFO['tileHG']) / height;
			if(scaleY == 0)
			{
				scaleY = canvasINFO['tileHG'] / height;
				posY = (canvasINFO['maxTY']-1)*canvasINFO['tileHG'];
			}
		}

		width = Math.floor(width*scaleX + 0.5);
		height = Math.floor(height*scaleY + 0.5);

		shape.set({width: width});
		shape.set({height: height});
		shape.set({scaleX: 1});
		shape.set({scaleY: 1});
		shape.set({left: posX});
		shape.set({top:posY});

		var text = regionsSet[shape.number].text;
		text.set({left: posX});
		text.set({top: posY});

		updateCoveredRes(shape);
		updateConn();
		updateObj();
	});
}



function addRegion(x,y,w,h,name,resources){

	var n = numberOfRegions + 1;
	regionName[n] = name;

	// compute region's data
	var region = new fabric.Rect({
	  left: x*canvasINFO['tileWG'],
	  top: (canvasINFO['maxTY']-h-y)*canvasINFO['tileHG'],
	  fill: 'rgba(255,255,255,0.6)',
	  width: w*canvasINFO['tileWG'],
	  height: h*canvasINFO['tileHG'],
	  strokeWidth:3,
	  stroke: 'rgba(255,0,0,1)',
	  hasRotatingPoint: false,
	  transparentCorners: false,
	  number: n
	});

	var text = new fabric.Text(' ' + name, {
		fontSize: 16,
		left: region.left,
		top: region.top,
		fill: 'black',
		hasControls: false,
		selectable: false
	});

	regionsSet[n] = {'active': true, 'text': text, 'region': region};

	// add region to canvas
	regionCanvas.add(region);
	regionCanvas.add(text);


	// add region stat fields
	var htmlFields = '<p><b>Region: ' + name + '</b></p>';
	htmlFields += '<table><tr><td>Required:</td>';

	for(var t in canvasINFO.resources)
		htmlFields += '<td>' + t + ' <input onkeyup="updateObj();" maxlength="4" class="readonly" readonly="readonly"' + 
						'onkeypress="return checkKey(event);" type="text" value="' + 
						(typeof resources[t] == 'undefined' ? 0 : resources[t]) + '" id="'+ n +'_' + t + '_r"></td>';

	htmlFields += '</tr><tr><td>Covered:</td>';

	for(var t in canvasINFO.resources)
		htmlFields += '<td>' + t + ' <input type="text" class="readonly" value="0" id="'+ n +'_' + t + '" readonly="readonly"></td>';
	htmlFields += '</tr></table>';
	$("#regionInfo").append(htmlFields);


	// update stat fields
	updateCoveredRes(region);

	numberOfRegions++;
}


// la prima riga � quella pi� in basso
function computeCoveredRes(region) {
	var x = region.left / canvasINFO['tileWG'];
	var y = region.top / canvasINFO['tileHG'];
	var w = region.width / canvasINFO['tileWG'];
	var h = region.height / canvasINFO['tileHG'];

	var res = {};
	for(var t in canvasINFO.resources)
		res[t] = 0;
	res['-F-'] = 0;
	res['NULL'] = 0;

	for(var col = x; col < x + w; col++) {
		for(var row = y; row < y + h; row++) {
			var t = canvasINFO['matrix'][canvasINFO['maxTY']-row-1][col];
			if(t == 'NULL' || t == '-F-')
				res[t] ++;
			else
				res[t] += canvasINFO['resNum'][t];
		}
	}

	return res;
}




function updateCoveredRes(region){
	res = computeCoveredRes(region);
	n = region.number;

	for(var t in canvasINFO.resources) {
		var id = '#' + n + '_' + t;
		$(id).val(res[t]);
	}
}



function generateInterconnectionsTable(){
	var table = '<table>';
	var selectFields = '';

	for(var i = 0; i <= 255; i++)
		selectFields += '<option value="'+i+'">'+i+'</option>';


	for(var reg = 2; reg <= numberOfRegions; reg++) {
		table += '<tr><td>' + regionName[reg] + '</td>';
		for(var reg2 = 1; reg2 < numberOfRegions; reg2++) {
			if(reg <= reg2)
				table += '<td></td>';
			else
				table += '<td><select onchange="updateConn()" id="'+ reg +'_'+reg2+'">'+selectFields+'</select></td>';
		}
		table += '</tr>';
	}

	table += '<tr><td></td>';
	for(var reg = 1; reg < numberOfRegions; reg++) {
		table += '<td>' + regionName[reg] + '</td>';
	}
	table += '</tr>';

	table += '</table>';

	$('#connectionsArea').append(table);

}


function initConn(connections) {
	for(var reg = 2; reg <= numberOfRegions; reg++) {
		for(var reg2 = 1; reg2 < numberOfRegions; reg2++) {
			if(reg <= reg2)
				continue;

		    linesCanvas.add(new fabric.Line([0,0,0,0],{
		    	left: 0,
		    	top: 0,
		    	width: 0,
		    	height: 0,
		    	strokeWidth: 1,
		    	selectable: false,
		        stroke: 'rgba(0,0,0,1)'
		    }));
		}
	}

	for(id in connections)
	{
		$('#' + id).val(connections[id]);
	}
}


function initResCost(initialCost) {
	var tableRes = '<table><tr>';

	for(var t in canvasINFO.resources) {
		tableRes += '<td>' + t + '</td>';
	}

	tableRes += '</tr><tr>';
	for(var t in canvasINFO.resources) {
		value = 1;
		if(typeof initialCost[t] !== 'undefined')
			value = initialCost[t]
		tableRes += '<td><input type="text" value="' + value + '" id="res_' + t + '" maxlength="4" onkeyup="updateObj();" onkeypress="return checkKey(event);"/></td>';
	}
	tableRes += '</tr></table>';

	$('#resCost').append(tableRes);
}


function updateConn() {

	// creazione delle nuove linee
	var index = -1;
	for(var reg = 2; reg <= numberOfRegions; reg++) {
		for(var reg2 = 1; reg2 < numberOfRegions; reg2++) {
			if(reg <= reg2)
				continue;
			var x1, y1, x2, y2, stroke;
			stroke = $('#' + reg + '_' + reg2).val();
			index++;
			var line = linesCanvas.item(index);

			if (stroke == 0) {
				line.left = -100;
				line.top = -100;
				line.width = 0;
				line.height = 0;
				continue;
			}

			x1 = regionsSet[reg]['region'].left + regionsSet[reg]['region'].width / 2;
			y1 = regionsSet[reg]['region'].top + regionsSet[reg]['region'].height / 2;

			x2 = regionsSet[reg2]['region'].left + regionsSet[reg2]['region'].width / 2;
			y2 = regionsSet[reg2]['region'].top + regionsSet[reg2]['region'].height / 2;

			var left = (x1 < x2 ? x1 : x2);
			var right = (x1 < x2 ? x2 : x1);
			var top = (y1 < y2 ? y1 : y2);
			var bottom = (y1 < y2 ? y2 : y1);
			var width = right - left;
			var height = bottom - top;
			if(height == 0) height = 0.1;
			if(width == 0) width = 0.1;

			line.left = left;
			line.top = top;
			line.width = width;
			line.height = height;
			line.strokeWidth = Math.ceil(stroke/64.0);

			if(x1 != x2 && (y2 - y1) / (x2 - x1) < 0)
			{
				line.scaleX = -1;
				line.left += line.width;
			}
			else
			{
				line.scaleX = 1;
			}
		}
	}
	linesCanvas.renderAll();
	updateObj();
}



function initSliders(initialW) {
	$("#sliderWL").slider({
      orientation: "vertical",
      range: "min",
      min: 0,
      max: 100,
      value: initialW['WL'],
      slide: function( event, ui ) {
        $( "#qWL" ).val( ui.value );
        updateObj();
      }
    });
    $( "#qWL" ).val(initialW['WL']);

    $("#sliderP").slider({
      orientation: "vertical",
      range: "min",
      min: 0,
      max: 100,
      value: initialW['P'],
      slide: function( event, ui ) {
        $( "#qP" ).val( ui.value );
        updateObj();
      }
    });
    $( "#qP" ).val(initialW['P']);

    $("#sliderR").slider({
      orientation: "vertical",
      range: "min",
      min: 0,
      max: 100,
      value: initialW['R'],
      slide: function( event, ui ) {
        $( "#qR" ).val( ui.value );
        updateObj();
      }
    });
    $( "#qR" ).val(initialW['R']);
}


function checkKey(pEvent)
{

	if (window.event) key=window.event.keyCode;
	else if(pEvent) key=pEvent.which;
	else return true;
	keychar=String.fromCharCode(key);

	// control keys
	if((key==null)||(key==0)||(key==8)||(key==9)||(key==27)) return true;


	// numbers
	if(("0123456789").indexOf(keychar) > -1) return true;

	return false;
}


function checker() {

	var interOk = true;
	var resOk = true;
	var forbiddenOk = true;

	// check for non intersection
	for(var reg = 2; reg <= numberOfRegions && interOk; reg++) {
		for(var reg2 = 1; reg2 < numberOfRegions && interOk; reg2++) {
			if(reg <= reg2)
				continue;
			var r1 = regionsSet[reg]['region'];
			var r2 = regionsSet[reg2]['region'];

			var maxX = Math.max(r1.left + r1.width, r2.left + r2.width);
			var maxY = Math.max(r1.top + r1.height, r2.top + r2.height);
			var minX = Math.min(r1.left, r2.left);
			var minY = Math.min(r1.top, r2.top);

			if(maxX - minX < r1.width + r2.width && maxY - minY < r1.height + r2.height)
				interOk = false;
		}
	}

	// check for covered resources
	for(var reg = 1; reg <= numberOfRegions; reg++) {
		for(var t in canvasINFO.resources) {
			var idCover = '#' + reg + '_' + t;
			var idReq = idCover + '_r';

			if(new Number($(idCover).val()) < new Number($(idReq).val())) {
				$(idCover).css('border-color','#FF0000');
				resOk = false;
			}
			else
				$(idCover).css('border-color','#00FF00');
		}
	}

	// check for overlapping with forbidden portions
	for(var reg = 1; reg <= numberOfRegions; reg++) {
		var r = regionsSet[reg]['region'];
		var res = computeCoveredRes(r);
		if(res['-F-'] > 0) {
			forbiddenOk = false;
			break;
		}
	}


	/*for(var p in canvasINFO.portions) {
		if(canvasINFO.portions.p[])
	}*/

	isValid = interOk && resOk && forbiddenOk;

	if(isValid)
	{
		$('#statusStr').text('Valid');
		$('#statusStr').css('color','#00FF00');
		$('#reason').text('');
	}
	else
	{
		$('#statusStr').text('Not valid');
		$('#statusStr').css('color','#FF0000');

		if(interOk && resOk && !forbiddenOk)
			$('#reason').text('(overlapping with forbidden areas)');
		else if(interOk && !resOk && forbiddenOk)
			$('#reason').text('(resource requirements not met)');
		else if(!interOk && resOk && forbiddenOk)
			$('#reason').text('(overlapping between regions)');
		else if(!interOk && !resOk && forbiddenOk)
			$('#reason').text('(overlapping between regions, resource requirements not met)');
		else if(!interOk && resOk && !forbiddenOk)
			$('#reason').text('(overlapping between regions and forbidden areas)');
		else if(interOk && !resOk && !forbiddenOk)
			$('#reason').text('(overlapping with forbidden areas, resource requirements not met)');
		else
			$('#reason').text('(overlapping between regions and forbidden areas, resource requirements not met)');
	}
}


function updateObj()
{
	checker();
	saveFloorplan();

	// do not show value if the floorplan is not valid
	if(!isValid)
	{
		$('#a_WL').val('');
		$('#a_P').val('');
		$('#a_R').val('');
		$('#n_WL').val('');
		$('#n_P').val('');
		$('#n_R').val('');
		$('#globalObj').val('');
		return;
	}

	var a_P = 0;
	var a_WL = 0;
	var a_R = 0;
	var n_P = 0.0;
	var n_WL = 0.0;
	var n_R = 0.0;
	var max_P = 0;
	var max_WL = 0;
	var max_R = 0;
	var globalObj = 0.0;
	var precision = 1000000;

	// compute HPWL
	for(var reg = 2; reg <= numberOfRegions; reg++) {
		for(var reg2 = 1; reg2 < numberOfRegions; reg2++) {
			if(reg <= reg2)
				continue;
			var x1, y1, x2, y2, bw;
			bw = $('#' + reg + '_' + reg2).val();

			x1 = (regionsSet[reg]['region'].left + regionsSet[reg]['region'].width / 2)*canvasINFO['tileW']/canvasINFO['tileWG'];
			y1 = (regionsSet[reg]['region'].top + regionsSet[reg]['region'].height / 2)*canvasINFO['tileH']/canvasINFO['tileHG'];

			x2 = (regionsSet[reg2]['region'].left + regionsSet[reg2]['region'].width / 2)*canvasINFO['tileW']/canvasINFO['tileWG'];
			y2 = (regionsSet[reg2]['region'].top + regionsSet[reg2]['region'].height / 2)*canvasINFO['tileH']/canvasINFO['tileHG'];

			a_WL += (Math.abs(x1-x2) + Math.abs(y1-y2))*bw;
			max_WL += ((canvasINFO['maxTX'])*canvasINFO['tileW'] + (canvasINFO['maxTY'])*canvasINFO['tileH'])*bw;
		}
	}

	// compute PERIMETER
	for(var reg = 1; reg <= numberOfRegions; reg++) {
		var shape = regionsSet[reg]['region'];
		a_P += (shape.width*canvasINFO['tileW']/canvasINFO['tileWG'] + shape.height*canvasINFO['tileH']/canvasINFO['tileHG'])*2;
	}
	max_P = (canvasINFO['maxTX']*canvasINFO['tileW'] + canvasINFO['maxTY']*canvasINFO['tileH'])*numberOfRegions*2;


	// compute WASTED RESOURCES
	var maxWasteVect = [];
	for(var t in canvasINFO.resources) {
		maxWasteVect[t] = maxRes[t];
	}

	for(var reg = 1; reg <= numberOfRegions; reg++) {
		for(var t in canvasINFO.resources) {
			var idCover = '#' + reg + '_' + t;
			var idReq = idCover + '_r';
			var required = new Number($(idReq).val());
			var waste = new Number($(idCover).val()) - required;

			maxWasteVect[t] -= required;
			a_R += waste*$('#res_' + t).val();
		}
	}

	for(t in canvasINFO.resources) {
		max_R += maxWasteVect[t]*$('#res_' + t).val();
	}


	n_P = (max_P == 0 ? n_P = 0 : n_P = a_P / max_P);
	n_WL = (max_WL == 0 ? n_WL = 0 : n_WL = a_WL / max_WL);
	n_R = (max_R == 0 ? n_R = 0 : n_R = a_R / max_R);

	qWL = $('#qWL').val() / 100.0;
	qP = $('#qP').val() / 100.0;
	qR = $('#qR').val() / 100.0;
	var qTot = qWL + qP + qR;
	if(qTot == 0)
	{
		globalObj = 0.0;
		qWL = 0;
		qP = 0;
		qR = 0;
	}
	else
	{
		qWL = qWL / qTot;
		qP = qP / qTot;
		qR = qR / qTot;
		globalObj = qWL * n_WL + qP * n_P + qR * n_R;
	}

	$('#a_WL').val(a_WL);
	$('#a_P').val(a_P);
	$('#a_R').val(a_R);
	$('#n_WL').val(Math.round(n_WL*precision)/precision);
	$('#n_P').val(Math.round(n_P*precision)/precision);
	$('#n_R').val(Math.round(n_R*precision)/precision);
	$('#globalObj').val(Math.round(globalObj*precision)/precision);
}



function optimize() 
{
	$('#console').html('');
	updateObj();
	App.consoleView();
    setTimeout(function(){App.doFloorplan(function(placement){
    	$('#dialog-floorplan-completed').dialog('open');
		$('#loading').hide();
		console.log(placement);
		for(var n = 1; n <= numberOfRegions; n++)
		{
			w = placement[n]['x2'] - placement[n]['x1'] + 1;
			h = placement[n]['y2'] - placement[n]['y1'] + 1;
			x = placement[n]['x1'];
			y = canvasINFO['maxTY'] - h - placement[n]['y1'];

			var shape = regionsSet[n]['region'];
			shape.left = x*canvasINFO['tileWG'];
			shape.top = y*canvasINFO['tileHG'];
			shape.width = w*canvasINFO['tileWG'];
			shape.height = h*canvasINFO['tileHG'];

			var text = regionsSet[shape.number].text;
			text.set({left: shape.left});
			text.set({top: shape.top});

			shape.setCoords();
			updateCoveredRes(shape);
		}
		regionCanvas.renderAll();
		updateConn();
		updateObj();
    });},300);    
}


/* ----------------- ajax call --------------- */

function saveFloorplan()
{
}

function sendSaveRequest()
{
}
