package it.polimi.scheduler;

public class Main {

	public static void main(String[] args) {
		
//		if(args.length != 3){
//			System.out.println("\n\t #### MISS PARAMETER ####");
//			return;
//		}
//		
//		String FPGAFile = null;
//		String XMLFile = null;
//		float decrement_factor = 0;
//		
//		for (int i = 0; i < args.length; i++) {
//			if(i == 0)
//				FPGAFile = args[i];
//			
//			if(i == 1)
//				XMLFile = args[i];
//			
//			if(i == 2)
//				decrement_factor = Float.parseFloat(args[i]);
//				
//		}
//		Scheduler sc = new Scheduler(decrement_factor, FPGAFile, XMLFile);
		
		java.util.Date startTime = new java.util.Date();		
		Scheduler sc = new Scheduler(0.1f, "XC7Z020.json", "tg_0_50.xml");		
		
		if(sc.scheduleTask() == 1){
			
			java.util.Date endTime = new java.util.Date();
			
			System.out.println("\t #### SOLUTION FOUND ####");
			
			long diff = endTime.getTime() - startTime.getTime();
			System.out.println("\t Time Used: " + diff + "ms");
			
		}else{
			
			System.out.println("\t #### TASKGRAPH EMPTY ####");
			
		}

	}

}
