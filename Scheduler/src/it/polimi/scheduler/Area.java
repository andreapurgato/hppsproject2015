package it.polimi.scheduler;

import java.util.ArrayList;
import java.util.List;

public class Area {
	
	//The id assigned to the area
	public int idArea;
	
	//Resources contained in this area
	public int clb;
	public int dsp;
	public int bram;
	
	//Task mapped into this area
	public List<Task> tasks;
	
	//Constructor of the area passing the id and that this area will contain
	public Area(int id, int clb, int dsp, int bram) {
		tasks = new ArrayList<Task>();
		this.idArea = id;
		this.clb = clb;
		this.dsp = dsp;
		this.bram = bram;
	}

}
