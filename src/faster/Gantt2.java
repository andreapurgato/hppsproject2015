package faster;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

class Gantt2 extends JFrame{

	public static final int X_MAX = 1900;
	public int max;
	
	public List<Task> taskToDraw;
	public List<Area> listArea;
	public Map<Area, Integer> mapArea;
	
	public Map<String, List<JButton>> mapButton;
	
    public Gantt2(List<Task> taskToDraw, List<Area> listArea){
    	
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	this.taskToDraw = taskToDraw;
    	this.listArea = listArea;
    	
    	mapArea = new HashMap<Area, Integer>();
    	
    	mapButton = new HashMap<String, List<JButton>>();
    	mapButton.put("cpu1", new ArrayList<JButton>());
    	mapButton.put("cpu2", new ArrayList<JButton>());
    	mapButton.put("rt", new ArrayList<JButton>());
    	for (Area area : listArea) {
    		mapButton.put("" + area.id, new ArrayList<JButton>());
		}
    	
    	int i = 220;
    	for (Area area : listArea) {
			mapArea.put(area, i);
			i+=70;
		}
    	
        JPanel panel=new JPanel();
        panel.setLayout(null);
        panel.setPreferredSize(new Dimension(1800, i));
        
        JScrollPane scroller = new JScrollPane(panel);
        scroller.getVerticalScrollBar().setUnitIncrement(10);
        getContentPane().add(scroller);
        setSize(X_MAX, 1000);
        
       
        createButtons(panel);
    }

    public float scale_x (int value) {
    	float val = value;
    	return (val / max) * (X_MAX - 200) + 100;
    }
    
    public void createButtons(JPanel panel) {
    	
    	if (taskToDraw.size() == 0) {
    		System.out.println("NO TASK AVAILABLE");
			return;
		}
    	
    	for (Task t : taskToDraw) {
			if (t.t_end > max) {
				max = t.t_end;
			}
		}
    	
    	//Assi Vericali
    	JSeparator line = new JSeparator(SwingConstants.VERTICAL);
    	line.setBounds(100, 0, 1, 5000);
    	panel.add(line);
    	
    	line = new JSeparator(SwingConstants.VERTICAL);
    	line.setBounds(1800, 0, 1, 5000);
    	panel.add(line);
    	///
    	
    	
        for (Task t : taskToDraw) {
        	
        	if ("sink".equals(t.idTask)) continue;
        	
        	if(t.t_start == 0 && t.t_end == 0)
        		continue;
        	
        	int ypsoln = 0;
        	String type = "";
        	
			if (t instanceof ReconfigurationTask) {
				ypsoln = 150;
				type = "rt";
			} else if (!t.hw) {
				
				if (t.cpu == 1) {
					type = "cpu1";
					ypsoln = 10;
				} else if (t.cpu == 2) {
					type = "cpu2";
					ypsoln = 80;
				}
				
				
			} else {
				ypsoln = mapArea.get(t.area_ref);
				type = "" + t.area_ref.id;
			}
			
			Rectangle2D r = new Rectangle2D.Double(scale_x(t.t_start), ypsoln, scale_x(t.t_end) - scale_x(t.t_start), 20);
			
			String critical_font = "";
			if (t.critical_path) critical_font = "color='red'";
			JButton b = new JButton("<html><font " + critical_font + " >" + t.idTask + "</font></html>");
			b.setBounds((int) (scale_x(t.t_start)), ypsoln, (int) (scale_x(t.t_end) - scale_x(t.t_start)), 20);
			b.setOpaque(false);
			b.setContentAreaFilled(false);
			b.setToolTipText("<html><font size='8' > " + 
					"ID: " + t.idTask + "<br>" +
					"IMPL: " + (t.hw ? "HW" : "SW") + "<br><br>" +
					"TMIN: " + t.t_min + "<br>" +
					"TMAX: " + t.t_max + "<br><br>" +
					"TSTART: " + t.t_start + "<br>" +
					"TEND: " + t.t_end + "<br>" +
					"EXE: " + t.execution_time + "</font></html>");
			panel.add(b);
			
			b = new JButton("<html><font " + critical_font + " >" + t.idTask + "</font></html>");
			b.setBounds((int) (scale_x(t.t_min)), ypsoln+20, (int) (scale_x(t.t_max) - scale_x(t.t_min)), 20);
			b.setOpaque(false);
			b.setContentAreaFilled(false);
			b.setToolTipText("<html><font size='8' > " + 
					"ID: " + t.idTask + "<br>" +
					"IMPL: " + (t.hw ? "HW" : "SW") + "<br><br>" +
					"TMIN: " + t.t_min + "<br>" +
					"TMAX: " + t.t_max + "<br><br>" +
					"TSTART: " + t.t_start + "<br>" +
					"TEND: " + t.t_end + "<br>" +
					"EXE: " + t.execution_time + "</font></html>");
			panel.add(b);
			
			line = new JSeparator(SwingConstants.HORIZONTAL);
	    	line.setBounds(100, ypsoln+55, 1700, 1);
	    	panel.add(line);
		}
        
        JLabel lab;
        
        for (Area area : listArea) {
        	lab = new JLabel("Area " + area.id);
        	lab.setBounds(40, (int) mapArea.get(area) + 15, 100, 10);
        	panel.add(lab);
		}
        
        lab = new JLabel("CPU 1");
    	lab.setBounds(40, 25, 100, 10);
    	panel.add(lab);
    	
        lab = new JLabel("CPU 2");
    	lab.setBounds(40, 95, 100, 10);
    	panel.add(lab);
    	
        lab = new JLabel("Reconf");
    	lab.setBounds(40, 165, 100, 10);
    	panel.add(lab);
        
    }

}