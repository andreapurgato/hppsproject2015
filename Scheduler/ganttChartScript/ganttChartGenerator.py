import matplotlib.pyplot as plt
import json
import sys

solution = sys.argv[1]

with open(solution) as data_file:    
    data = json.load(data_file)

#Offset bar
offsetBar = 0.2

#Bars color
colorSW = [0, 0, 1, 1]
colorHW = [1, 0, 0, 1]
colorReconfig = [0, 1, 0, 1]


#Create Y labels
resNames = []
resNames.append('Reconfiguration')
labelsNumber = 2

#Get Area Labels
for area in data["areas"]:
    resNames.append("Area " + str(area["id_area"]))
    labelsNumber +=1

#Append cpu's labels
resNames.append('Cpu 2')
resNames.append('Cpu 1')
labelsNumber +=2

#Y coordinates
Ycpu1 = labelsNumber - offsetBar - 1
Ycpu2 = labelsNumber - offsetBar - 2

#Get last clock cycle
maxCloclCycle = 0
for task in data["tasks"]:
    if (task["id_task"] == "sink"):
        maxCloclCycle = task["t_end"]

fig, ax = plt.subplots(figsize = (15, labelsNumber/2))
ax.grid(True)

#Limite coordinata Y
ax.set_ylim(0, labelsNumber)

#Numero di elementi sull'asse Y
ax.set_yticks(range(1, labelsNumber))

ax.set_yticklabels(resNames)
ax.set_xlabel('clock cycles')
ax.set_xlim(0, maxCloclCycle)

for task in data["tasks"]:
    
    if (task["id_task"] == "sink"):
        continue
    
    #Get start and end time of task
    start = task["t_start"]
    end = task["t_end"]
    lenght = end - start
    
    #Check if the task is SW or HW
    if (task["implementation"]["type"] == "SW"):
        
        ## SW TASK ##        
        
        #Set the color
        color = colorSW
        
        #Get CPU number to calculate y coordinate
        if (task["cpu_ref"] == 1):
            ypsoln = Ycpu1
        else:
            ypsoln = Ycpu2
            
    else:

        ## HW TASK ##  
        
        #Set the color
        color = colorHW        
        
        #Get the Area number to calculate y coordinate
        ypsoln = task["area_ref"] - offsetBar + 2;
				
    ax.annotate(task["id_task"], xy = (start, ypsoln + 0.5), size = 10)
    ax.broken_barh([(start, lenght)] , (ypsoln, 0.4), facecolors=color)


for reconfig in data["reconfigurations"]:
    
    start = reconfig["t_start"]
    end = reconfig["t_end"]
    lenght = end - start
    ypsoln = 1 - offsetBar
    
    ax.annotate(reconfig["prev_task"], xy = (start, ypsoln + 0.5), size = 10)
    ax.annotate(reconfig["next_task"], xy = (start, ypsoln - 0.3 ), size = 10)
    ax.broken_barh([(start, lenght)] , (ypsoln, 0.4), facecolors=colorReconfig)

plt.savefig(".\output\gantt\ganttChart.png")     
#plt.show()



#for areas in data["areas"]:
#    resNames.append(areas["tasks_area"][0]["id_task"]);
