"""
This script generate an html view of all the results
"""

from  lib.parser import *
from problems.problems import problems 
import lib.db as db
import re


# read the template
with open("web_view_files/view.html") as f:
	template = f.read()


for problem in problems:

	problem_path = 'results/' + problem['name']
	for filename in os.listdir(problem_path):
		result = re.match('(.*)\.sol$',filename)
		if result:
			alg_name = result.group(1)

			with open(problem_path + '/' + filename) as f:
				solData = eval(f.read())

			print 'generating: ' + problem['name'] + ' ' + alg_name

			htmlFile = open('results/' + problem['name'] + '/' + alg_name + '.html','w')
			htmlData = template


			# generate regions data
			resources = {r[0] : {'CLB' : int(r[1] / 2.0 + 0.5) , 'BRAM' : r[2], 'DSP' : r[3]} for r in problem['regions']}
			regionsStr = ''

			for r in solData:
				recNameOrig = str(r)
				recName = recNameOrig[0:recNameOrig.find('_')]
				w = solData[r]['w'] + 1
				h = solData[r]['y2'] - solData[r]['y1'] + 1
				x = solData[r]['x']
				y = solData[r]['y1'] - 1
				regionsStr += 'addRegion(' + str(x) + ',' + str(y) + ',' + str(w) + ',' + str(h) + ',"' + recNameOrig + '",' + str(resources[recName]) + ')\n'

			htmlData = htmlData.replace('{{{regions}}}',regionsStr);


			htmlData = htmlData.replace('{{{connections}}}','')

			htmlFile.write(htmlData)
			htmlFile.close()

