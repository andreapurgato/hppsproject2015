package faster;

import java.util.ArrayList;
import java.util.List;

public class Task {

	public String idTask;
	
	public int execution_time;
	public boolean hw;
	
	public List<Integer> execution_list_hw;
	public List<Integer> lut_list; //le lut nell'xml si chiamano area
	public List<Integer> dsp_list;
	public List<Integer> bram_list;
	public List<Integer> ff_list;
	public List<Integer> clb_list;
	public List<Integer> execution_list_sw;
	public List<Integer> efficiency_implementation_hw;
	
	public Area area_ref;
	
	public boolean critical_path;
	
	public int clb;
	public int dsp;
	public int bram;
	public int efficiency;
	
	public int t_min;
	public int t_max;
	
	int t_start;
	int t_end;
	
	// 0 = nulla; 1 = cpu1; 2 = cpu2;
	public int cpu = 0;
	
	public int slack;
	
	public List<Task> inGoing;
	public List<Task> outGoing;
	
	public boolean updatedForDelayReconfiguration;
	
	public Task () {
		inGoing = new ArrayList<Task>();
		outGoing = new ArrayList<Task>();
		execution_list_hw = new ArrayList<Integer>();
		execution_list_sw = new ArrayList<Integer>();
		lut_list = new ArrayList<Integer>();
		dsp_list  = new ArrayList<Integer>();
		bram_list = new ArrayList<Integer>();
		ff_list = new ArrayList<Integer>();
		clb_list = new ArrayList<Integer>();
		efficiency_implementation_hw = new ArrayList<Integer>();
	}
	
	public void extractImplementationBestRatio (int weightClb, int weightDsp, int weightBram, int ff_in_clb, int lut_in_clb) {
		
		
		if (!execution_list_hw.isEmpty()) {
			
			for (int i = 0; i < execution_list_hw.size(); i++) {
				
				if (ff_list.get(i)/ff_in_clb > lut_list.get(i)/lut_in_clb) {
					clb_list.add(ff_list.get(i)/ff_in_clb);
				} else {
					clb_list.add(lut_list.get(i)/lut_in_clb);
				}
				
				int efficiency = 
						execution_list_hw.get(i) / (weightClb*clb_list.get(i) +
						weightDsp*dsp_list.get(i) +
						weightBram*bram_list.get(i));
				
				efficiency_implementation_hw.add(efficiency);
				
			}
			
			//Prendiamo l'implementazione pi� efficiente tra quelle calcolate su
			int min = efficiency_implementation_hw.get(0);
			int min_index = 0;
			
			for (int i = 0; i < efficiency_implementation_hw.size(); i++) {
				
				if (efficiency_implementation_hw.get(i) > min) {
					min = efficiency_implementation_hw.get(i);
					min_index = i;
					hw = true;
				}
				
			}
			
			execution_time = execution_list_hw.get(min_index);
			clb = clb_list.get(min_index);
			dsp = dsp_list.get(min_index);
			bram = bram_list.get(min_index);
			efficiency = efficiency_implementation_hw.get(min_index);
			hw = true;
		}
		
		for (int i = 0; i < execution_list_sw.size(); i++) {
			if (execution_list_sw.get(i) < execution_time || execution_time == 0) {
				execution_time = execution_list_sw.get(i);
				clb = 0;
				dsp = 0;
				bram = 0;
				efficiency = 0;
				hw = false;
			}
		}
		
	}
	
	public void switchToSoftwareImplementation(){
		
		execution_time = execution_list_sw.get(0);
		clb = 0;
		dsp = 0;
		bram = 0;
		efficiency = 0;
		hw = false;
		
		for (int i = 0; i < execution_list_sw.size(); i++) {
			if (execution_list_sw.get(i) < execution_time) {
				execution_time = execution_list_sw.get(i);
			}
		}
		
	}
	
	public boolean switchToHardwareImplementation(){
		
		if(efficiency_implementation_hw.isEmpty())
			return false;
		
		//Prendiamo l'implementazione pi� efficiente tra quelle calcolate su
		int min = efficiency_implementation_hw.get(0);
		int min_index = 0;
		
		for (int i = 0; i < efficiency_implementation_hw.size(); i++) {
			
			if (efficiency_implementation_hw.get(i) > min) {
				min = efficiency_implementation_hw.get(i);
				min_index = i;
				hw = true;
			}
			
		}
		
		execution_time = execution_list_hw.get(min_index);
		clb = clb_list.get(min_index);
		dsp = dsp_list.get(min_index);
		bram = bram_list.get(min_index);
		efficiency = efficiency_implementation_hw.get(min_index);
		hw = true;
		
		return true;
		
	}

}
