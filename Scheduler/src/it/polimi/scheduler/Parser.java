package it.polimi.scheduler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author Davide Tantillo
 * @author Andre Purgato
 */
public class Parser {

	public int getReconfigurationTime (String fileXML) {
		
		try {
			
			//Opening xml file and creating the reader
			File fXmlFile = new File(fileXML);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			//Retrieving the reconfiguration time value into the xml file
			NodeList recTime = doc.getElementsByTagName("reconfiguration_controller");
			return Integer.parseInt(((Element) (recTime.item(0).getChildNodes().item(1)) ).getAttribute("value"));
				
			} catch (Exception e) {
				e.printStackTrace();
		    }
		
		//Reconfiguration time not found
		return -1;
		
	}
	
	public List<Task> createTaskList (String fileXML) {
		
		//Instantiating an empty Task ArrayList
		List<Task> listTask = new ArrayList<Task>();
		
		try {
			
			//Opening xml file and creating the reader
			File fXmlFile = new File(fileXML);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			//Put the element 'partition' of xml in a NodeList where are contained all the names of the tasks
			//and the precedence between them
			Element elementPartition = (Element) doc.getElementsByTagName("partition").item(0);
			
			//Put the tasks name in a NodeList
			NodeList allTasks = elementPartition.getElementsByTagName("task");
			
			//Put the precedence between tasks in a NodeList
			NodeList allPrecedences = elementPartition.getElementsByTagName("transfer");
			
			//Put all the implementations of the task in a NodeList
			NodeList allImplementations = doc.getElementsByTagName("implementation");
			
			//Creates the list of task without information about implementation and precedence
			for (int taskIndex = 0; taskIndex < allTasks.getLength(); taskIndex++) {
				
				//Extract the taskIndex-th task from the allTasks list
				Element taskElement = (Element) allTasks.item(taskIndex);
		 
				//Create a new task and putting it into the list of tasks
				listTask.add(new Task(taskElement.getAttribute("id")));
				
			}
			
			//Assign to every task all its implementations
			assingImplementations(listTask, allImplementations);
			
			//Assign to every task all its predecessors and successors
			assignPreedences(listTask, allPrecedences);
			
			
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		
		return listTask;
	}

	
	private void assingImplementations(List<Task> listTasks, NodeList allImplementations) {

		//Scan the implementations NodeList in order to add it to the corresponding task
		for (int implIndex = 0; implIndex < allImplementations.getLength(); implIndex++) {
			
			//Extract the implIndex-th implementation from the allTasks list
			Element implementationElement = (Element) allImplementations.item(implIndex);
			NodeList taskInImplementation = implementationElement.getElementsByTagName("task");
			
			//An implementation can be assigned to different tasks, so I need to scan assign
			//the implementation to all its respective tasks
			List<String> taskInImplementationList = new ArrayList<String>();
			for (int taskIndex = 0; taskIndex < taskInImplementation.getLength(); taskIndex++) {
				String idTask = ((Element) taskInImplementation.item(taskIndex)).getAttribute("id");
				taskInImplementationList.add(idTask);
			}
			
			//Get the name of the task of the implementation
			for (String taskOfImplementationId : taskInImplementationList) {
				
				//Searching the task referred to the current implementation
				Task currentTask = null;
				for (Task task : listTasks) {
					if (taskOfImplementationId.equals(task.idTask)) {
						currentTask = task;
						break;
					}
				}
				
				//Get the HW/SW type of the implementation and assigning it to the task
				Element typeOfImplementationTag = (Element) implementationElement.getElementsByTagName("component").item(0);
				String typeOfimplementationString = typeOfImplementationTag.getAttribute("type");
				currentTask.hw = "HW".equals(typeOfimplementationString);
				
				//Get the all specification information of the implementation
				NodeList specs = implementationElement.getElementsByTagName("specs");
				
				//Scan all the specification and assigning them to the task
				for (int i = 0; i < specs.getLength(); i++) {
					
					//Get the name and the value of the specification
					String name = ((Element) specs.item(i)).getAttribute("name");
					int value = Integer.parseInt(((Element) specs.item(i)).getAttribute("value"));
					
					//If the task is hw, add it to the hw list
					if (currentTask.hw) {
						
						if ("execution_time".equals(name)) {
							currentTask.execution_list_hw.add(value);
						} else if ("area".equals(name)) {
							currentTask.lut_list.add(value);
						} else if ("FFs".equals(name)) {
							currentTask.ff_list.add(value);
						} else if ("DSPs".equals(name)) {
							currentTask.dsp_list.add(value);
						} else if ("BRAMs".equals(name)) {
							currentTask.bram_list.add(value);
						}
						
					//If the task is sw, add it to the sw list	
	                } else {
	                	if ("execution_time".equals(name)) {
	                		currentTask.execution_list_sw.add(value);
	                		currentTask.lut_list.add(0);
						}
	                }
					
				}
				
				//Checking if every list has been filled. Otherwise it's needed to fill it with 0's to
				//keep information aligned with all the lists
				int size = currentTask.execution_list_hw.size();
				if (currentTask.lut_list.size() < size) {
					currentTask.lut_list.add(0);
				}
				if (currentTask.ff_list.size() < size) {
					currentTask.ff_list.add(0);
				}
				if (currentTask.dsp_list.size() < size) {
					currentTask.dsp_list.add(0);
				}
				if (currentTask.bram_list.size() < size) {
					currentTask.bram_list.add(0);
				}
					
			}
				
		}
			
	}
	
	private void assignPreedences(List<Task> listTask, NodeList allPrecedences) {
		
		//Scan the precedence NodeList in order to add all the precedences to the corresponding task
		for (int precedenceIndex = 0; precedenceIndex < allPrecedences.getLength(); precedenceIndex++) {
			
			//Extract the precedenceIndex-th implementation from the allTasks list
			Element elementPrecedence = (Element)allPrecedences.item(precedenceIndex);
			
			//Get the source and target of the current precedence
			String src = elementPrecedence.getAttribute("src");
			String tgt = elementPrecedence.getAttribute("tgt");
			
			//Searching the respective task to the source and target
			Task srcTask = null;
			Task tgtTask = null;
			
			for (Task task : listTask) {
				if (tgt.equals(task.idTask + ".in")) {
					tgtTask = task;
				}
				
				if (src.equals(task.idTask + ".out")) {
					srcTask = task;
				}
			}
			
			//Assigning the target to the outGoing task list of source
			srcTask.outGoing.add(tgtTask);
			
			//Assigning the source to the inGoing task list of target
			tgtTask.inGoing.add(srcTask);
		}
		
	}
	
}
