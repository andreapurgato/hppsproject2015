#
# SCHEDULER
#

To execute the scheduler follow these instructions:
 1 - Put the JSON related to the FPGA on the fpga folder.
 2 - Put the XML related to the taskgraph on the taskgraph folder
 3 - Execute the following command in the main folder of the scheduler
 		
 		java -jar scheduler.jar fpga.json taskgraph.xml decrement_factor
 		
 		Where:
 			fpga.json is the JSON file of the FPGA
 			taskgraph.xml is the XML file of the taskgraph
 			decrement_factor is the decrement factor that the algorithm apply on each iteration if the solution is infeasible
 		 