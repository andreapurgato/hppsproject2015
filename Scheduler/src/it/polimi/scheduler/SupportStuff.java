package it.polimi.scheduler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * TODO: Javadoc
 * 
 * @author Andrea Purgato
 * @author Davide Tantillo
 *
 */

public class SupportStuff {
	
	public SupportStuff() { }
	
	/**
	 * Function used to check if the solution found respect constraints.
	 */
	public boolean checkValidity(List<Task> tasksList, List<Area> areasList, List<Task> reconfigurationTaskskList, List<Task> cpu1, List<Task> cpu2){
		
		boolean precedence = true;
		
		// Check if the precedence respect the precedence with predecessors and successors
		for (Task task : tasksList) {
			
			if (!task.inGoing.isEmpty()) {
				for (Task inTask : task.inGoing) {
					
					if(inTask.t_end > task.t_start){
						System.out.println("VALIDITY ERROR: " + task.idTask + " violate the precedence on INGOING task: " + inTask.idTask);
						precedence = false;
					}
					
				}
			}
			
			if (!task.outGoing.isEmpty()) {
				for (Task outTask : task.outGoing) {
					
					if (outTask.t_start < task.t_end) {
						System.out.println("VALIDITY ERROR: " + task.idTask + " violate the precedence on OUTGOING task: " + outTask.idTask);
						precedence = false;
					}
					
				}
			}
		}
			
		boolean reconfPrecedence = true;
		
		// Check if each reconfiguration task is exectuted between the tasks that generate the reconfiguration
		for (Task reconfigurationTask : reconfigurationTaskskList) {
			
			if (reconfigurationTask.inGoing.get(0).t_end > reconfigurationTask.t_start) {
				System.out.println("VALIDITY ERROR: " + reconfigurationTask.idTask + " violate the precedence on INGOING task: " + reconfigurationTask.inGoing.get(0).idTask);
				reconfPrecedence = false;					
			}
			
			if (reconfigurationTask.outGoing.get(0).t_start < reconfigurationTask.t_end) {
				System.out.println("VALIDITY ERROR: " + reconfigurationTask.idTask + " violate the precedence on OUTGOING task: " + reconfigurationTask.inGoing.get(0).idTask);
				reconfPrecedence = false;	
			}
			
		}
		
		boolean cpuPrecedence = true;
		
		// Check if the task SW on the CPU1 and on the CPU2 are not overlapped
		for (int i = 0; i < cpu1.size()-1; i++) {
			
			Task prev = cpu1.get(i);
			Task next = cpu1.get(i+1);
			
			if (prev.t_end > next.t_start) {
				System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on CPU1 with task: " + next.idTask);
				cpuPrecedence = false;
			}
			
		}
		
		for (int i = 0; i < cpu2.size()-1; i++) {
			
			Task prev = cpu2.get(i);
			Task next = cpu2.get(i+1);
			
			if (prev.t_end > next.t_start) {
				System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on CPU2 with task: " + next.idTask);
				cpuPrecedence = false;
			}
			
		}	
		
		boolean areaPrecedence = true;
		
		// Check if the tasks on each area are not overlapped
		for (Area area : areasList) {
			for (int i = 0; i < area.tasks.size()-1; i++) {
				
				Task prev = area.tasks.get(i);
				Task next = area.tasks.get(i+1);
				
				if (prev.t_end > next.t_start) {
					System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on area " + area.idArea+ " with task: " + next.idTask);
					areaPrecedence = false;
				}
				
			}
		}
		
		// Check if during the reconfiguration tasks are not overlapped
		if (reconfigurationTaskskList != null) {
			
			for (int i = 0; i < reconfigurationTaskskList.size()-1; i++) {
				
				Task prev = reconfigurationTaskskList.get(i);
				Task next = reconfigurationTaskskList.get(i+1);
				
				if (prev.t_end > next.t_start) {
					System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on RECONFIGURATION with reconf task: " + next.idTask);
					reconfPrecedence = false;
				}
				
			}
			
		}
		
		return (precedence && reconfPrecedence && cpuPrecedence && areaPrecedence);
		
	}
	
	/**
	 * Function that generate the JSON that the florplanner need to check the feasibility.
	 * TODO: Commentare il codice.
	 */
	@SuppressWarnings("unchecked")
	private void outputJsonGenerator(List<Area> areasList){
		
		JSONObject outputJson = new JSONObject();
		JSONObject region_data = new JSONObject();
		
		for (Area area : areasList) {
			
			JSONObject region = new JSONObject();
			region.put("io", new JSONArray());
			
			JSONObject resources = new JSONObject();
			
			if (area.clb > 0) 
				resources.put("CLB", area.clb);
			
			if (area.bram > 0) 
				resources.put("BRAM", area.bram);
			
			if (area.dsp > 0)
				resources.put("DSP", area.dsp);
			
			region.put("resources", resources);
			
			region_data.put("rec" + area.idArea, region);
		
		}
		
		outputJson.put("regions_data", region_data);
		
		//Communications array
		outputJson.put("communications", new JSONArray());
		
		//Weight of communications object
		JSONObject obj_weights = new JSONObject();
		obj_weights.put("wirelength", 0);
		obj_weights.put("perimeter", 0);
		obj_weights.put("resources", 0);
		outputJson.put("obj_weights", obj_weights);
		
		//Cost of resources
		JSONObject res_cost = new JSONObject();
		res_cost.put("CLB", 1);
		res_cost.put("BRAM", 1);
		res_cost.put("DSP", 1);
		outputJson.put("res_cost", res_cost);
		
		outputJson.put("placement_generation_mode", "irreducible");
		
		JSONObject gurobi_params = new JSONObject();
		gurobi_params.put("MIPFocus", 1);
		outputJson.put("gurobi_params", gurobi_params);

        Writer output = null;
        String text = outputJson.toString();
        File file = new File("floorplanner/output.json");
        
        try {	
        	
        	output = new BufferedWriter(new FileWriter(file));
			output.write(text);
			output.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Function that launch the floorplanner and check if the solution found is feasible.
	 */
	@SuppressWarnings("resource")
	public boolean checkFeasibility(List<Area> areasList, String JSONFpga){
		
		// Creating output JSON
		outputJsonGenerator(areasList);
		
		// Creating the runtime environment
		Runtime rt = Runtime.getRuntime();
        Process pr = null;
        
        // Launch the floorplanner
        try {
        	
			pr = rt.exec("cmd /c python floorplanner/floorplanner_cl.py floorplanner/output.json fpga/" + JSONFpga);
			
			// Wait the end of the process
			pr.waitFor();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

        // Reading the response of the floorplanner
        File file = new File("result.json");
        BufferedReader input = null;
       
        String text = "";
        StringBuffer buffer = null;
        
        // Reading the json file generated by the floorplanner
        try {
        	
        	input = new BufferedReader(new FileReader(file));
            buffer = new StringBuffer();
            
			while ((text = input.readLine()) != null)
				buffer.append(text + "\n");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        // Parsing the result to cjeck the feasibility
        JSONParser parser = new JSONParser();
        JSONObject resultJson = null;
        try {
        	resultJson = (JSONObject) parser.parse(buffer.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        return (boolean) resultJson.get("status");
        
	}
	
	/**
	 * Function that create the JSON file for the Gannt drawer and draw the Gannt.
	 */
	public void generateGannt(){
		// Creating the runtime environment
		Runtime rt = Runtime.getRuntime();
        
        // Launch the floorplanner
        try {
			Process pr = rt.exec("cmd /c python ganttChartScript/ganttChartGenerator.py output/solution.json");
			pr.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Function used to generate the JSON with all the areas created.
	 */
	@SuppressWarnings("unchecked")
	public void generateResultJson(List<Area> areasList, List<Task> tasksList, List<Task> reconfigurationTasksList){
		
		JSONObject outputJson = new JSONObject();
		
		// Adding areas to the JSON file
		JSONArray areasArray = new JSONArray();
		
		for (Area area : areasList) {
			
			JSONObject areaJson = new JSONObject();
			areaJson.put("id_area", area.idArea);
			areaJson.put("clb", area.clb);
			areaJson.put("dsp", area.dsp);
			areaJson.put("bram", area.bram);
			
			JSONArray tasksArray = new JSONArray();
			int index = 0;
			for (Task task : area.tasks) {
				
				JSONObject taskAreaJson = new JSONObject();
				taskAreaJson.put("id_task", task.idTask);
				
				tasksArray.add(index, taskAreaJson);
				index++;
				
			}
			
			areaJson.put("tasks_area", tasksArray);
			
			areasArray.add(area.idArea, areaJson);
			
		}
		
		// Adding tasks to the JSON file
		JSONArray tasksArray = new JSONArray();
		
		for (Task task : tasksList) {
			
			JSONObject taskJson = new JSONObject();
			taskJson.put("id_task", task.idTask);
			taskJson.put("t_start", task.t_start);
			taskJson.put("t_end", task.t_end);
			if(task.hw)
				taskJson.put("area_ref", task.area_ref.idArea);
			else
				taskJson.put("cpu_ref", task.cpu);
			
			
			JSONObject implementation = new JSONObject();
			if(task.hw) {
				
				implementation.put("type", "HW");
				implementation.put("clb", task.clb);
				implementation.put("dsp", task.dsp);
				implementation.put("bram", task.bram);
				implementation.put("execution_time", task.execution_time);
				implementation.put("efficiency", task.efficiency);
				
			} else {
				
				implementation.put("type", "SW");
				implementation.put("execution_time", task.execution_time);
				
			}
			
			taskJson.put("implementation", implementation);
			
			tasksArray.add(taskJson);
			
		}
		
		// Adding reconfiguration task to the JSON file
		JSONArray reconfigurationTasksArray = new JSONArray();
		
		for (Task task : reconfigurationTasksList) {
			
			JSONObject recJson = new JSONObject();
			recJson.put("prev_task", task.inGoing.get(0).idTask);
			recJson.put("next_task", task.outGoing.get(0).idTask);
			recJson.put("area", task.area_ref.idArea);
			recJson.put("execution_time", task.execution_time);
			recJson.put("t_start", task.t_start);
			recJson.put("t_end", task.t_end);
			
			reconfigurationTasksArray.add(recJson);
			
		}
		
		outputJson.put("areas", areasArray);
		outputJson.put("tasks", tasksArray);
		outputJson.put("reconfigurations", reconfigurationTasksArray);
		
		Writer output = null;
        String text = outputJson.toString();
        File file = new File("output/solution.json");
        
        try {	
        	
        	output = new BufferedWriter(new FileWriter(file));
			output.write(text);
			output.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Function that generate the HTML viewer.
	 */
	public void generateHTMLViewer(String FPGAModel, int clb, int dsp , int bram, List<Task> tasksList, List<Area> areasList, int reconfNumber){
		
		StringBuilder htmlPage = new StringBuilder();
		htmlPage.append("<html>");
		// Head
		htmlPage.append("<head><title>Solution Web View</title><link rel=\"stylesheet\" type=\"text/css\" href=\"webviewstyle/style.css\"></head>");
		
		htmlPage.append("<body>");
		
		// Div with FPGA characteristics
		htmlPage.append("<div id=\"divfpga\">");
		
		htmlPage.append("<h2>FPGA CHRACTERISTICS</h2><br>");
		
		htmlPage.append("FPGA Model: " + FPGAModel + "<br>");
		htmlPage.append("CLB: " + clb + "<br>");
		htmlPage.append("DSP: " + dsp + "<br>");
		htmlPage.append("BRAM: " + bram + "<br>");
		
		int clb_used = 0;
		int dsp_used = 0;
		int bram_used = 0;
		
		for (Area area : areasList) {
			clb_used += area.clb;
			dsp_used += area.dsp;
			bram_used += area.bram;
		}
		
		htmlPage.append("CLB USED: " + (clb_used * 100 / clb) + "%<br>");
		htmlPage.append("DSP USED: " + (dsp_used * 100 / dsp) + "%<br>");
		htmlPage.append("BRAM USED: " + (bram_used * 100 / bram) + "%<br>");
		
		htmlPage.append("</div>");
		
		// Div with Task data
		htmlPage.append("<div id=\"divtask\">");
		htmlPage.append("<h2>TASKS DATA</h2><br>");
		htmlPage.append("<table>");
		
		htmlPage.append("<tr>");
		
		htmlPage.append("<td> <b>ID TASK</b> </td>");
		htmlPage.append("<td> <b>T START</b> </td>");
		htmlPage.append("<td> <b>T END</b> </td>");
		htmlPage.append("<td> <b>EXECUTION TIME</b> </td>");
		htmlPage.append("<td> <b>IMPLEMENTATION</b> </td>");
		htmlPage.append("<td> <b>CLB</b> </td>");
		htmlPage.append("<td> <b>DSP</b> </td>");
		htmlPage.append("<td> <b>BRAM</b> </td>");
		htmlPage.append("<td> <b>AREA</b> </td>");
		htmlPage.append("<td> <b>CPU</b> </td>");
		
		htmlPage.append("</tr>");
		
		int total_exe = 0;
		
		for (Task task : tasksList) {
			
			if(task.t_end > total_exe)
				total_exe = task.t_end;
			
			if(task.idTask.equals("sink"))
				continue;
			
			htmlPage.append("<tr>");
			
			htmlPage.append("<td>" + task.idTask + "</td>");
			htmlPage.append("<td>" + task.t_start + "</td>");
			htmlPage.append("<td>" + task.t_end + "</td>");
			htmlPage.append("<td>" + task.execution_time + "</td>");
			
			if(task.hw)
				htmlPage.append("<td> HW </td>");
			else
				htmlPage.append("<td> SW </td>");
			
			htmlPage.append("<td>" + task.clb + "</td>");
			htmlPage.append("<td>" + task.dsp + "</td>");
			htmlPage.append("<td>" + task.bram + "</td>");
			
			if(task.hw)
				htmlPage.append("<td> " + task.area_ref.idArea + " </td>");
			else
				htmlPage.append("<td> - </td>");
			
			htmlPage.append("<td>" + task.cpu + "</td>");
			
			htmlPage.append("</tr>");
			
		}
		
		htmlPage.append("</table>");
		
		htmlPage.append("<h3>TOTAL EXECUTION TIME: " + total_exe + " clock cycle</h3><br>");
		htmlPage.append("<h3>AREAS GENERATED: " + areasList.size() + "</h3><br>");
		htmlPage.append("<h3>RECONFIGURATION NEEDED: " + reconfNumber + "</h3><br>");
		
		htmlPage.append("</div>");
		
		// Div with Areas data
		htmlPage.append("<div id=\"divarea\">");
		htmlPage.append("<h2>AREAS DATA</h2><br>");
		htmlPage.append("<table>");
		
		htmlPage.append("<tr>");
		
		htmlPage.append("<td> <b>ID AREA</b> </td>");
		htmlPage.append("<td> <b>CLB AREA</b> </td>");
		htmlPage.append("<td> <b>DSP AREA</b> </td>");
		htmlPage.append("<td> <b>BRAM AREA</b> </td>");
		htmlPage.append("<td> <b>TASKS ON AREA</b> </td>");
		
		htmlPage.append("</tr>");
		
		for (Area area : areasList) {
			
			htmlPage.append("<tr>");
			
			htmlPage.append("<td>" + area.idArea + "</td>");
			
			htmlPage.append("<td>" + area.clb + "</td>");
			htmlPage.append("<td>" + area.dsp + "</td>");
			htmlPage.append("<td>" + area.bram + "</td>");
			
			htmlPage.append("<td>");
			for (Task task : area.tasks) {
				htmlPage.append(task.idTask + "<br>");
			}
			htmlPage.append("</td>");
			
			htmlPage.append("</tr>");
			
		}
		
		htmlPage.append("</table>");
		htmlPage.append("</div>");
		
		// Gannt chart
		htmlPage.append("<div id=\"divgantt\">");
		htmlPage.append("<h2>GANTT CHART</h2><br>");
		htmlPage.append("<img src=\"gantt/ganttChart.png\" />");
		htmlPage.append("</div>");
		
		htmlPage.append("</html>");
		
		// Write it on file
		Writer output = null;
        String text = htmlPage.toString();
        File file = new File("output/webView.html");
        
        try {	
        	
        	output = new BufferedWriter(new FileWriter(file));
			output.write(text);
			output.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}
