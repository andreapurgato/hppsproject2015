package faster;

import java.util.ArrayList;
import java.util.List;

public class Area {
	
	public int id;
	public int clb;
	public int dsp;
	public int bram;
	public List<Task> tasks;
	
	
	public Area(int id, int clb, int dsp, int bram) {
		tasks = new ArrayList<Task>();
		this.id = id;
		this.clb = clb;
		this.dsp = dsp;
		this.bram = bram;
	}

}
