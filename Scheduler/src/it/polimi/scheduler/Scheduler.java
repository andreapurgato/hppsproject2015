package it.polimi.scheduler;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * TODO: JavaDoc
 * 
 * @author Andrea Purgato
 * @author Davide Tantillo
 *
 */

public class Scheduler {
	
	public static final int FRAME_PER_CLB = 36;
	
	/**
	 * Parameters that represent the FPGA chosen to run the tasks.
	 */
	public int ff_per_clb;
	public int lut_per_clb;
	public int clb_fpga;
	public int dsp_fgpa;
	public int bram_fpga;
	
	/**
	 * Parameters that represent the dimension of a single tile in the FPGA chosen to run the tasks.
	 */
	public int clb_min;
	public int dsp_min;
	public int bram_min;
	
	/**
	 * Parameters that represent the resources still available in the FPGA chosen to run the tasks.
	 */
	public int clb_available;
	public int dsp_available;
	public int bram_available;
	
	/**
	 * Parameters that represent the weight of each resource.
	 * They are used to calculate the efficiency of each HW implementation.
	 */
	public int clb_weight;
	public int dsp_weight;
	public int bram_weight;
	
	/**
	 * Speed used to reconfigure the areas.
	 */
	public int reconf_time;
	
	/**
	 * Alpha that define the factor for the resources when the solution found is not feasible.
	 * Decrement factor for the resources.
	 */
	public float alpha;
	public float decrement_factor;
	
	/**
	 * Support object used to parse the XML file and the JSON file.
	 */
	public ParserFPGA parser_fpga;
	public Parser parser_tasks;
	public String XMLFile;
	public String JSONFpga;
	
	/**
	 * Lists used to contain the tasks, the reconfiguration tasks and the areas created.
	 */
	public List<Task> tasksList;
	public List<Task> reconfigurationTasksList;
	public List<Area> areasList;
	
	
	/**
	 * Lists used to keep track of the tasks on each CPU.
	 */
	public static List<Task> cpu1;
	public static List<Task> cpu2;
	
	/**
	 * Class with some extra function that are used to do extra stuff.
	 * Generating result JSON for the floorplanner.
	 * Checking the validity.
	 * Generating the JSON for the Gannt Drawer.
	 */
	SupportStuff stuff;
	
	
	public Scheduler(float decrement_factor, String JSONFpga, String XMLFile){
		
		this.decrement_factor = decrement_factor;
		
		tasksList = new ArrayList<Task>();
		reconfigurationTasksList = new ArrayList<Task>();
		areasList = new ArrayList<Area>();
		
		cpu1 = new ArrayList<Task>();
		cpu2 = new ArrayList<Task>();
		
		stuff = new SupportStuff();
		
		// Reading the FPGA characteristics
		this.JSONFpga = JSONFpga;
		print("Reading FPGA chracteristics...");
		parser_fpga = new ParserFPGA(JSONFpga);
		setFpgaParameter();
		print("FPGA Model: " + parser_fpga.fpga_model + "\n");
		
		this.XMLFile = XMLFile;
		parser_tasks = new Parser();
		
	}
	
	public int scheduleTask(){
		
		boolean feasible = false;
		int iteration = 1;
		
		while(!feasible){
		
			print("ITERATION " + iteration + " - Alpha = " + alpha + " - Decrement Factor = " + decrement_factor + "\n");
			
			// Taking the reconfiguration speed
			reconf_time = parser_tasks.getReconfigurationTime("taskgraph/" + XMLFile);
			
			// Creating the Tasks list
			print("Creating tasks list...");
			tasksList = parser_tasks.createTaskList("taskgraph/" + XMLFile);
			print("Task number: " + tasksList.size() + "\n");
			
			if(tasksList.isEmpty())
				return -1;
			
			// Ordering the task topologically
			print("Topological ordering...\n");
			topologicalOrdering();
			
			print("Extracting best implementation...\n");
			extractBestImplementation();
			
			// Calculating the execution window of each task
			windowCalculation();
			
			// Calculating the slack of each task
			print("Critical path extraction...\n");
			slackCalculator();
			
			// Find the critical path
			tasksList.get(0).critical_path = true;
			findCriticalPath(tasksList.get(0));
	
			// Define the areas for the critic tasks before and for non critic task after
			print("Defining areas...");
			areasDefinitionForCritic(efficiencyOrdering());
			areasDefinitionForNonCritic(efficiencyOrdering());
			taskAreaOrdering();
			print("Areas created: " + areasList.size() + "\n");
			
			// Recomputing the window
			windowCalculation();
			
			// Try to balance SW tasks with HW tasks
			print("Balancing tasks...\n");
			balanceTasks();
			taskAreaOrdering();
			
			// Recomputing again the window
			windowCalculation();
			
			// Computing the T START and the T END
			print("Start and End instants computation...\n");
			startEndCalculation();
			
			// Creation of the Reconfiguration Tasks
			print("Reconfiguration Tasks creation...\n");
			reconfigurationTaskCreation();
			
			// Scheduling the SW Tasks
			print("Scheduling SW Tasks...\n");
			scheduleTaskSoftware();
			
			// Schedule the Reconfiguration Task of the critical path
			print("Scheduling Reconfiguration Tasks critical...\n");
			List<Task> newReconfigurationTasksList = scheduleReconfigurationTaskCritical(reconfigurationSearilization(true));
			
			// Schedule the Reconfiguration Task not critical
			print("Scheduling Reconfiguration Tasks not critical...\n");
			reconfigurationTasksList = scheduleReconfigurationTaskNonCritical(reconfigurationSearilization(false), newReconfigurationTasksList);
			
			print("Checking validity of the solution...");
			boolean validity = stuff.checkValidity(tasksList, areasList, reconfigurationTasksList, cpu1, cpu2);
			print("Validity response: " + validity + "\n");
			
			print("Checking feasibility...");
			feasible = stuff.checkFeasibility(areasList, JSONFpga);
			print("Feasibility response: " + feasible + "\n");
			
			if(!feasible)
				alpha = alpha * (1 - decrement_factor);
			
			iteration++;
			
		}
		
		print("Generating JSON result...\n");
		stuff.generateResultJson(areasList, tasksList, reconfigurationTasksList);
		
		print("Generating Gannt...\n");
		stuff.generateGannt();
		
		print("Generating web view...\n");
		stuff.generateHTMLViewer(parser_fpga.fpga_model, clb_fpga, dsp_fgpa, bram_fpga, tasksList, areasList, reconfigurationTasksList.size());
		
		return 1;
		
	}
	
	private void print(String s){
		java.util.Date date= new java.util.Date();
		System.out.format("%-25s | %-50s", new Timestamp(date.getTime()), s);
		System.out.println();
	}
	
	/**
	 * Function used to get the parameters of the FPGA from the parser class.
	 */
	private void setFpgaParameter(){
		
		ff_per_clb = parser_fpga.ff_per_clb;
		lut_per_clb = parser_fpga.lut_per_clb;
		
		clb_fpga = parser_fpga.clb_fpga;
		dsp_fgpa = parser_fpga.dsp_fgpa;
		bram_fpga = parser_fpga.bram_fpga;
		
		clb_min = parser_fpga.clb_min;
		dsp_min = parser_fpga.dsp_min;
		bram_min = parser_fpga.bram_min;
		
		clb_weight = 100 - ((100 * clb_fpga) / (clb_fpga + dsp_fgpa + bram_fpga));
		dsp_weight = 100 - ((100 * dsp_fgpa) / (clb_fpga + dsp_fgpa + bram_fpga));
		bram_weight = 100 - ((100 * bram_fpga) / (clb_fpga + dsp_fgpa + bram_fpga));
		
		alpha = 1;
	}
	
	/**
	 * Function that extract the best implementation of each task.
	 */
	private void extractBestImplementation(){
		
		for (Task task : tasksList) {
			task.extractBestImplementation(clb_weight, dsp_weight, bram_weight, ff_per_clb, lut_per_clb);
		}
		
	}
	
	/**
	 * Function that order topologically the tasks of the graph.
	 */
	private void topologicalOrdering(){
		
		int size = tasksList.size();
		
		List<String> order = new ArrayList<String>();
		List<Task> orderedTask = new ArrayList<Task>();
		List<Task> taskss = parser_tasks.createTaskList("taskgraph/" + XMLFile);	
		
		
		Task temp = null;
		while (order.size() < size) {
			
			for (Task t : tasksList) {
				
				if (t.inGoing.size() == 0) {
					temp = t;
					break;
				}
				
			}
			
			order.add(temp.idTask);
			tasksList.remove(temp);

			for (Task t : tasksList) {
				
				if (t.inGoing.contains(temp)) {
					t.inGoing.remove(temp);
				}
				
			}
			
		}
		
		
		
		for (String string : order) {
			
			for (Task task : taskss) {
				if (task.idTask.equals(string)) {
					orderedTask.add(task);
					break;
				}
			}
			
		}
		
		Task sink = new Task("sink");
		
		for (Task task : orderedTask) {
			if (task.outGoing.isEmpty()) {
				sink.inGoing.add(task);
				task.outGoing.add(sink);
			}
		}
		
		orderedTask.add(sink);
		
		tasksList = orderedTask;
	}
	
	
	/**
	 * Function that calculate T MIN and T MAX for each task.
	 */
	private void windowCalculation(){
		
		// Calculating T MIN
		tasksList.get(0).t_min = 0;
		for (int i = 1; i < tasksList.size(); i++) {
			
			Task tempTask = tasksList.get(i);
			int time = 0;
			
			// Looking for the maximum T MIN + EXECUTION TIME of the the ingoing tasks
			for (Task t : tempTask.inGoing) {
				if (time < t.execution_time + t.t_min)
					time = t.execution_time + t.t_min;
					
			}
			
			tempTask.t_min = time;
		}
		
		// Calculating T MAX
		tasksList.get(tasksList.size()-1).t_max = tasksList.get(tasksList.size()-1).t_min;
		for (int i = tasksList.size()-2; i >= 0; i--) {
			
			Task tempTask = tasksList.get(i);
			int time = tempTask.outGoing.get(0).t_max - tempTask.outGoing.get(0).execution_time;
			
			// Looking for the minimum T MAX - EXECUTION TIME of the the outgoing tasks
			for (Task t : tempTask.outGoing) {
				if (time > t.t_max - t.execution_time)
					time = t.t_max - t.execution_time;
			}
			
			tempTask.t_max = time;
		}
		
	}
	
	/**
	 * Function that calculate the slack of each task.
	 */
	private void slackCalculator () {
		
		for (Task task : tasksList) {
			task.slack = task.t_max - task.t_min - task.execution_time;
		}
		
	}
	
	/**
	 * Function that calculate the critical path, setting the task that are in as critic.
	 * In case of more critical path the function take the first one available.
	 */
	private void findCriticalPath (Task task) {
		
		if (task.outGoing.isEmpty()) {
			return;
		}
		
		for (Task outGoingTask : task.outGoing) {
			if (outGoingTask.slack == 0) {
				outGoingTask.critical_path = true;
				findCriticalPath(outGoingTask);
				break;
			}
		}
		
	}
	
	private List<Task> efficiencyOrdering(){
		
		List<Task> tasksEfficencyOrderedList = new ArrayList<Task>();
		
		for (Task task : tasksList) {
			int i = 0;
			
			// If the task is not HW, skip it
			if(!task.hw)
				continue;
			
			// Finding the position where the task should go in the ordered array
			for (Task t : tasksEfficencyOrderedList) {
				if (task.efficiency > t.efficiency) {
					break;
				}
				i++;
			}
			
			// Add in that position.
			tasksEfficencyOrderedList.add(i, task);
		}
		
		return tasksEfficencyOrderedList;
		
	}
	
	/**
	 * Function that check if a given task is overlapped with the tasks already mapped on a given area
	 */
	private boolean noConflictsWindow(Area area, Task task){
		
		for (Task taskInArea : area.tasks) {
						
			// Overlap with task that start after
			if ((task.t_min <= taskInArea.t_max && task.t_min >= taskInArea.t_min && task.t_max >= taskInArea.t_max)) {
				return false;
			}
			
			// Overlap with task that start before
			if ((task.t_min <= taskInArea.t_min && task.t_max >= taskInArea.t_min && task.t_max <= taskInArea.t_max)) {
				return false;
			}
			
			// Overlap with task that start before and end after
			if ((task.t_min <= taskInArea.t_min && task.t_max >= taskInArea.t_max)) {
				return false;
			}
			
			// Overlap with nested task
			if ((task.t_min >= taskInArea.t_min && task.t_max <= taskInArea.t_max)) {
				return false;
			}
			
		}
		
		// No overlap
		return true;
		
	}
	
	/**
	 * Function that check if the task has window overlapped with the task of the given area.
	 * The difference with the function noConflictWindow is that here take into account the window defined by the execution time and not the window defined by T MAX
	 */
	private boolean noConflictsBalance(Area area, Task task){
		
		for (Task taskInArea : area.tasks) {
						
			// Overlap with task that start after
			if ((task.t_min <= (taskInArea.t_min + taskInArea.execution_time) && task.t_min >= taskInArea.t_start && task.t_max >= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
			
			// Overlap with task that start before
			if ((task.t_min <= taskInArea.t_min && task.t_max >= taskInArea.t_start && task.t_max <= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
			
			// Overlap with task that start before and end after
			if ((task.t_min <= taskInArea.t_min && task.t_max >= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
			
			// Overlap with nested task
			if ((task.t_min >= taskInArea.t_min && task.t_max <= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
		}
		return true;
		
	}
	
	
	/**
	 * Function that check if a task can be mapped in the available resources.
	 */
	private boolean spaceAvailable(Task task){
		
		int used_clb = 0;
		int used_dsp = 0;
		int used_bram = 0;
		
		if (task.clb != 0) {
			used_clb = (task.clb/clb_min + 1) * clb_min;
		}
		
		if (task.dsp != 0) {
			used_dsp = (task.dsp/dsp_min + 1) * dsp_min;
		}
		
		if (task.bram != 0) {
			used_bram = (task.bram/bram_min + 1) * bram_min;
		}
		
		return	used_clb <= clb_available &&	used_dsp <= dsp_available &&	used_bram <= bram_available;
		
	}
	
	/**
	 * Function that create areas where map the critical tasks
	 */
	private void areasDefinitionForCritic(List<Task> efficiencyOdered){
		
		int id_area = 0;
		
		// Calculating the resources available
		clb_available = (int) (clb_fpga * alpha);
		dsp_available = (int) (dsp_fgpa * alpha);
		bram_available = (int) (bram_fpga * alpha);
		
		// Parsing the tasks to create areas, only the task critical
		for (Task task : efficiencyOdered) {
			
			if (!task.hw || !task.critical_path) continue;
			
			boolean found = false;
			int minClb = 0;
			int minDsp = 0;
			int minBram = 0;
			Area usedArea = null;
			
			// FIRST: look for the minimum existing area where put the critic task
			for (Area area : areasList) {
				if (
					area.clb >= task.clb &&
					area.dsp >= task.dsp &&
					area.bram >= task.bram &&
					noConflictsWindow(area, task) &&
					(area.clb < minClb || area.dsp < minDsp || area.bram < minBram || !found)) {
					
						usedArea = area;
						minClb = area.clb;
						minDsp = area.dsp;
						minBram = area.bram;
						found = true;
						
				}
			}

			if (found) {
				usedArea.tasks.add(task);
				task.area_ref = usedArea;
			}
			
			// SECOND: if area not found try to create one new
			if (spaceAvailable(task) && !found) {
				
				int used_clb = 0;
				int used_dsp = 0;
				int used_bram = 0;
				
				// Calculating the space used by the task on the FPGA (number of tile of each resource)
				if (task.clb != 0) {
					used_clb = (task.clb/clb_min + 1) * clb_min;
				}
				
				if (task.dsp != 0) {
					used_dsp = (task.dsp/dsp_min + 1) * dsp_min;
				}
				
				if (task.bram != 0) {
					used_bram = (task.bram/bram_min + 1) * bram_min;
				}
				
				task.area_ref = new Area(id_area, used_clb, used_dsp, used_bram);
				areasList.add(task.area_ref);
				task.area_ref.tasks.add(task);
				clb_available -= used_clb;
				dsp_available -= used_dsp;
				bram_available -= used_bram;
				id_area++;
				
			} else if (!found){
				
				// THIRD: if the FPGA has not enough space, switch the implementation to SW.
				task.switchToSoftwareImplementation();
			}
			
		}
		
	}
	
	/**
	 * Function that create areas where map the non critical tasks
	 */
	private void areasDefinitionForNonCritic(List<Task> efficiencyOdered){
		
		int id_area = areasList.get(areasList.size() - 1).idArea + 1;
		
		// Parsing the tasks to create areas, only the task critical
		for (Task task : efficiencyOdered) {
			
			if (!task.hw || task.critical_path) continue;
			
			boolean found = false;
			int minClb = 0;
			int minDsp = 0;
			int minBram = 0;
			Area usedArea = null;
			
			// FIRST: if area not found try to create one new
			if (spaceAvailable(task) && !found) {
				
				int used_clb = 0;
				int used_dsp = 0;
				int used_bram = 0;
				
				// Calculating the space used by the task on the FPGA (number of tile of each resource)
				if (task.clb != 0) {
					used_clb = (task.clb/clb_min + 1) * clb_min;
				}
				
				if (task.dsp != 0) {
					used_dsp = (task.dsp/dsp_min + 1) * dsp_min;
				}
				
				if (task.bram != 0) {
					used_bram = (task.bram/bram_min + 1) * bram_min;
				}
				
				task.area_ref = new Area(id_area, used_clb, used_dsp, used_bram);
				areasList.add(task.area_ref);
				task.area_ref.tasks.add(task);
				clb_available -= used_clb;
				dsp_available -= used_dsp;
				bram_available -= used_bram;
				id_area++;
				
			} else {
				
				// SECOND: look for the minimum existing area where put the critic task
				for (Area area : areasList) {
					if (
						area.clb >= task.clb &&
						area.dsp >= task.dsp &&
						area.bram >= task.bram &&
						noConflictsWindow(area, task) &&
						(area.clb < minClb || area.dsp < minDsp || area.bram < minBram || !found)) {
							usedArea = area;
							minClb = area.clb;
							minDsp = area.dsp;
							minBram = area.bram;
							found = true;
					}
				}
				
				if (found) {
					usedArea.tasks.add(task);
					task.area_ref = usedArea;
				}
				
				if (!found){
					// THIRD: if the FPGA has not enough space, switch the implementation to SW.
					task.switchToSoftwareImplementation();
				}
			}
				
		}
			
	}	
	
	/**
	 * Function that order for each area the tasks mapped on it, looking at their T MIN.
	 */
	private void taskAreaOrdering(){
		
		for (Area area : areasList) {
			
			List<Task> areaTasks = area.tasks;
			area.tasks = new ArrayList<Task>();
			
			for (Task task : areaTasks) {
				int j = 0;
				for (Task taskComodo : area.tasks) {
					if (task.t_min < taskComodo.t_min) {
						break;
					}
					j++;
				}
				area.tasks.add(j, task);
			}
		}
				
	}
	
	/**
	 * Function that has the aim to balance the task SW with the task HW finding a solution that use both techniques.
	 */
	private void balanceTasks(){
		
		List<Task> softwareTaskList = new ArrayList<Task>();
		
		// Taking all the SW task sorted by T MIN
		for (Task task : tasksList) {
			int i = 0;
			if (!task.hw && !"sink".equals(task.idTask)) {
				
				for (Task swTask : softwareTaskList) {
					if (task.t_min < swTask.t_min) {
						break;
					}
					i++;
				}
				
				softwareTaskList.add(i, task);
			}
		}
		
		// Going backward through the SW task trying to decrease the total time switching the lasts SW tasks in HW
		for (int i = softwareTaskList.size()-1; i >= 0; i--) {
			
			Task aux = softwareTaskList.get(i);
			
			// Take the estimated reconfigration time
			int estimatedReconfTimeTotal = estimateReconfTime();
			
			for (Area area : areasList) {
				
				// If the Task end before the limit imposed by the estimated reconfiguration task skip the task
				if(aux.t_min + aux.execution_time < estimatedReconfTimeTotal)
					continue;
				
				// If the Task can be mapped on the current area, map it
				if (area.clb >= aux.clb && area.dsp >= aux.dsp && area.bram >= aux.bram && noConflictsBalance(area, aux)) {
					
					// The switch to the HW implementation occur
					if(aux.switchToHardwareImplementation()){
						aux.area_ref = area;
						area.tasks.add(aux);
					}					
					break;
					
				}
				
			}
		
		}
	}
	
	/**
	 * Function that estimate the total reconfiguration time needed in each area.
	 * It return the average reconfiguration time of each area multiplied by the number of reconfigurations necessary.
	 */
	private int estimateReconfTime(){
		
		if(areasList.isEmpty())
			return 0;
		
		int avg = 0;
		int count = 0;
		
		// Calculation of the average reconfiguration time
		for (Area area : areasList) { 
			
			// Reconfiguration time of the area
			int aux = (area.clb * reconf_time / clb_min * 36) + (area.dsp * reconf_time / dsp_min) + (area.bram * reconf_time / bram_min);
			
			if (avg == 0) {
				avg = aux;
			} else {
				avg += aux;
				avg = avg / 2;
			}
			
			count += (area.tasks.size() - 1);
			
		}
		
		// Estimating the time spent to reconfigure areas
		return avg * count;
		
	}	
	
	/**
	 * Function that for each task calculate the T START and the T END.
	 */
	private void startEndCalculation(){
		
		for (Task task : tasksList) {
			
			// The task start as soon as possible, so at the T MIN time instant
			task.t_start = task.t_min;			
			task.t_end = task.t_start + task.execution_time;
			
			// Check if delay is generated
			if (task.t_end > task.t_max) {
				
				// Add the delay to every task that is in the subgraph starting from the current task (the one that generate delay)
				addDelayToNextTask(task, task.t_end - task.t_max);
				
				// Reset the field used to understand if a Task has been delayed
				for (Task t : tasksList) {
					t.updatedForDelayReconfiguration = false;
				}
			}
			
		}
		
	}
	
	/**
	 * Recursive function used to delay each task.
	 * This function sum the delay only if the Task that generate delay is overlapped with following tasks.
	 */
	private void addDelayToNextTask(Task startingTask, int delay){
		
		// Terminate condition: if no task follow the startingTask (that generate delay) return
		if (startingTask.outGoing.isEmpty()) {
			return;
		}
		
		// Foreach outgoing check if overlapp
		for (Task task : startingTask.outGoing) {
			
			// Check if the delay of startingTask cause overlap with the follower Task
			if (startingTask.t_end < task.t_min) {
				continue;
			}
			
			// New delay generated on the following task
			int newDelay = startingTask.t_end - task.t_min + 1;
			
			// Adding the delay to the task
			task.t_min += newDelay;
			task.t_max += newDelay;
			task.t_start += newDelay;
			task.t_end += newDelay;
			
			// If the task is SW the algorithm has to shift all the Tasks on the CPU where the Task is running
			if(!task.hw){
				if(task.cpu == 1)
					shiftTaskSoftware(cpu1, task.t_start - newDelay);
				else
					shiftTaskSoftware(cpu2, task.t_start - newDelay);
			}
			
			// The delay is generated, so propagate it to the follower (on the subtree)
			addDelayToNextTask(task, newDelay);
		}
		
	}
	
	/**
	 * Function that has the aim to shift all the SW on a specific CPU if a Task on that CPU generate delay.
	 * The task are shifted only in case of overlapping.
	 */
	private void shiftTaskSoftware(List<Task> cpuTask, int limit){
		
		// For each task on the CPU check if a shift is required after a delay
		for (int i = 0; i < cpuTask.size()-1; i++) {
			
			Task task = cpuTask.get(i);
			Task nextTask = cpuTask.get(i+1);
			
			// If the task is before the Task that initially generate delay skip it
			if(nextTask.t_start <= limit)
				continue;
			
			// If the delay doesn't generate overlap skip
			if(task.t_end < nextTask.t_min)
				continue;
			
			// Overlap is generated, so delay calculation and summation to current task
			int delay = task.t_end - nextTask.t_min + 1;
			
			nextTask.t_min += delay;
			nextTask.t_max += delay;
			nextTask.t_start += delay;
			nextTask.t_end += delay;
			
			// Propagate the delay on the subtree of the current task
			addDelayToNextTask(task, delay);
		}
		
	}
	
	/**
	 * Function that generate the reconfiguration tasks.
	 */
	private void reconfigurationTaskCreation () {
		
		for (Area area : areasList) {
			for (int i = 0; i < area.tasks.size()-1; i++) {
				
				// Creating the new reconfiguration task
				Task reconfigTask = new Task("");
				reconfigTask.t_min = area.tasks.get(i).t_min + area.tasks.get(i).execution_time + 1;
				reconfigTask.t_max = area.tasks.get(i+1).t_min - 1;
				reconfigTask.inGoing.add(area.tasks.get(i));
				reconfigTask.outGoing.add(area.tasks.get(i+1));
				reconfigTask.area_ref = area;
				
				// Calculating the reconfiguration time
				reconfigTask.execution_time = (area.clb * reconf_time / clb_min * FRAME_PER_CLB) + (area.dsp * reconf_time / dsp_min) + (area.bram * reconf_time / bram_min);
				
				reconfigTask.idTask = reconfigTask.inGoing.get(0).idTask + " ---> " + reconfigTask.outGoing.get(0).idTask;
				
				// If the outgoing task is critical, so the reconfiguration task is critical
				if (reconfigTask.outGoing.get(0).critical_path) {
					reconfigTask.critical_path = true;
				}
				
				reconfigurationTasksList.add(reconfigTask);
				
			}
		}
		
	}
	
	/**
	 * Function that schedule each SW task over the two CPU, trying to parallelize at most as possible the execution over the two CPU.
	 */
	private void scheduleTaskSoftware() {
		
		List<Task> softwareTaskList = new ArrayList<Task>();
		
		// Getting all the SW Tasks in time order
		for (Task task : tasksList) {
			int i = 0;
			if (!task.hw && !"sink".equals(task.idTask)) {
				
				for (Task swTask : softwareTaskList) {
					if (task.t_min < swTask.t_min) {
						break;
					}
					i++;
				}
				
				softwareTaskList.add(i, task);
			}
		}
		
		// Temporary variable to kepp track the last task on each CPU and the possible delay on each CPU if the task to add generate delay.
		Task lastTaskCpu1 = null;
		Task lastTaskCpu2 = null;
		int delayCpu1;
		int delayCpu2;
		
		// CPU 1 has the priority over the CPU2
		for (Task task : softwareTaskList) {
			
			// If the CPU1 is empty the task goes on CPU1 (delay = 0)
			if (cpu1.isEmpty()) {
				delayCpu1 = 0;
			} else {
				
				// If the CPU1 has already Tasks get the last and calculate the delay
				lastTaskCpu1 = cpu1.get(cpu1.size()-1);
				delayCpu1 = lastTaskCpu1.t_end - task.t_min + 1;
				
				// If delay is negative set it to 0
				if (delayCpu1 < 0) {
					delayCpu1 = 0;
				}
			}
			
			// If CPU2 is empty the task goes on CPU2 (delay = 0)
			if (cpu2.isEmpty()) {
				delayCpu2 = 0;
			} else {
				
				// If the CPU2 has already Tasks get the last and calculate the delay
				lastTaskCpu2 = cpu2.get(cpu2.size()-1);
				delayCpu2 = lastTaskCpu2.t_end - task.t_min + 1;
				
				// If delay is negative set it to 0
				if (delayCpu2 < 0) {
					delayCpu2 = 0;
				}
			}
			
			int delay = 0;
			
			// Check the CPU that generate minor delay and add the task to it
			if (delayCpu1 <= delayCpu2) {
				cpu1.add(task);
				task.cpu = 1;
				delay = delayCpu1;
			} else {
				cpu2.add(task);
				task.cpu = 2;
				delay = delayCpu2;
			}
			
			// Calculate T START and T END of the current Task
			task.t_start = task.t_min + delay;
			task.t_end = task.t_start + task.execution_time;
			
			// Sum the delay that the CPU chosen generated
			task.t_min += delay;
			task.t_max += delay;
			
			// If more daly is generated by the execution sum it to the delay already generated
			if (task.t_end > task.t_max) {
				delay += (task.t_end - task.t_max); 
			}
			
			if (delay>0) {
				
				// Add the delay to the subtree of the current Task
				addDelayToNextTask(task, delay);
				
				// Recompute eventually T MIN and T MAX of each reconfiguration task, the delay of one task could generate delay on the Reconf Task
				for (Task rt : reconfigurationTasksList) {
					rt.t_min = rt.inGoing.get(0).t_max;
					rt.t_max = rt.outGoing.get(0).t_min;
				}
				
				// Reset the field to check the update
				for (Task t : tasksList) {
					t.updatedForDelayReconfiguration = false;
				}

			}
		}
	}
	
	/**
	 * Function that filter the reconfiguration tasks looking for the ones that verify the critical condition or not.
	 */
	private List<Task> reconfigurationSearilization (boolean critical) {
		
		List<Task> serializedTasks = new ArrayList<Task>();
		
		for (Task current_rec : reconfigurationTasksList) {
			
			// If a reconfiguration task doesn't verify the critical condition, skip it
			if (current_rec.critical_path != critical)
				continue;
			
			// Find the position of the current reconfiguration task, to have a ordered list
			int i = 0;
			for (Task rec : serializedTasks) {
				if (current_rec.t_min < rec.t_min) {
					break;
				}
				i++;
			}
			serializedTasks.add(i, current_rec);
		}
		
		return serializedTasks;
	}
	
	/**
	 * Function that schedule each Reconfiguration Task Critical
	 */
	private List<Task> scheduleReconfigurationTaskCritical(List<Task> reconfigurationTasksListCritical){
		
		List<Task> newReconfigurationTaskList = new ArrayList<Task>();
		
		// If NO reconfiguration task critical return NULL
		if (reconfigurationTasksListCritical.isEmpty()) {
			return new ArrayList<Task>();
		}
		
		// Take the first reconfiguration task and calculate T START and T END
		Task rec_task = reconfigurationTasksListCritical.get(0);
		rec_task.t_start = rec_task.t_min;
		rec_task.t_end = rec_task.t_start + rec_task.execution_time;
		
		// If the execution of the first reconfiguration generate delay, propagate it
		if (rec_task.t_end > rec_task.t_max) {
			
			int delay = rec_task.t_end - rec_task.t_max;
			
			addDelayToNextTask(rec_task, delay);
			
			//Ricalcolo dei tmin e tmax dei reconfiguration time
			for (Task rt : reconfigurationTasksList) {
				rt.t_min = rt.inGoing.get(0).t_max;
				rt.t_max = rt.outGoing.get(0).t_min;
			}
			
			for (Task task : tasksList) {
				task.updatedForDelayReconfiguration = false;
			}
		}
		
		// Add the current reconfiguration task to the new list of reconfiguration task
		newReconfigurationTaskList.add(rec_task);
		
		// Now that we have the first reconfiguration task scheduled, do the same to others
		for (Task reconfigurationTask : reconfigurationTasksListCritical) {
			
			if (newReconfigurationTaskList.contains(reconfigurationTask))
				continue;

			// Compute the T START and T END of the current reconfiguration task
			reconfigurationTask.t_min = reconfigurationTask.inGoing.get(0).t_end;
			reconfigurationTask.t_max = reconfigurationTask.outGoing.get(0).t_start;
			
			// Take the last reconfiguration task scheduled from the new list
			Task previousRecTask = newReconfigurationTaskList.get(newReconfigurationTaskList.size()-1);
			
			// Check where the current reconfiguration task can start
			if (reconfigurationTask.t_min > previousRecTask.t_end) {
				
				// It can start at his T MIN (no overlap)
				reconfigurationTask.t_start = reconfigurationTask.t_min;
				reconfigurationTask.t_end = reconfigurationTask.t_start + reconfigurationTask.execution_time;
			} else {
				
				// It has to start at last reconfiguration task T END (overlap)
				reconfigurationTask.t_start = previousRecTask.t_end + 1;
				reconfigurationTask.t_end = reconfigurationTask.t_start + reconfigurationTask.execution_time;
			}

			// Check if delay is generated, if yes propagate it
			if (reconfigurationTask.t_end > reconfigurationTask.t_max) {
				
				int delay = reconfigurationTask.t_end - reconfigurationTask.t_max;
				
				addDelayToNextTask(reconfigurationTask, delay);
				
				//Ricalcolo dei tmin e tmax dei reconfiguration time
				for (Task rt : reconfigurationTasksList) {
					rt.t_min = rt.inGoing.get(0).t_max;
					rt.t_max = rt.outGoing.get(0).t_min;
				}
				
				for (Task task : tasksList) {
					task.updatedForDelayReconfiguration = false;
				}
			}
			
			// Add the reconfiguration task to the new list
			newReconfigurationTaskList.add(reconfigurationTask);
			
		}
		
		return newReconfigurationTaskList;
		
	}
	
	/**
	 * Function that schedule each Reconfiguration Task NON Critical
	 */
	private List<Task> scheduleReconfigurationTaskNonCritical(List<Task> reconfigurationTasksListNonCriticalList, List<Task> criticalReconfiguraionTaskList){
		
		// List that will contain al the Reconfiguration Tasks scheduled sorted
		List<Task> finalReconfigurationTasksLikst = new ArrayList<Task>();
		
		// If no reconfiguration task non critical, return
		if (criticalReconfiguraionTaskList.isEmpty()) {
			return new ArrayList<Task>();
		}
		
		// Add all the reconfiguration tasks critical to the final list
		for (Task reconfigurationTask : criticalReconfiguraionTaskList) {
			finalReconfigurationTasksLikst.add(reconfigurationTask);
		}
		
		// For each non critic reconfiguration task find the first available window where schedule it
		for (Task nonCriticTask : reconfigurationTasksListNonCriticalList) {
			
			int delay = 0;
			boolean inserted = false;
			
			// Recalculate the T MIN and T MAX of the reconfiguration task, looking at the Task before and after the reconfiguration
			nonCriticTask.t_min = nonCriticTask.inGoing.get(0).t_end;
			nonCriticTask.t_max = nonCriticTask.outGoing.get(0).t_start;

			// Find the first available windows where schedule the current reconfiguration task 
			for (int i = 0; i < finalReconfigurationTasksLikst.size()-1; i++) {
				
				int start_point = 0;
				
				// Getting two by two the reconfiguration task already scheduled
				Task prev = finalReconfigurationTasksLikst.get(i);
				Task next = finalReconfigurationTasksLikst.get(i+1);
				
				// Check if between the two reconfiguration task already scheduled there is a window good to insert the current reconfiguration task
				if (nonCriticTask.t_min >= prev.t_start && nonCriticTask.t_min < next.t_start) {
					
					/*
					 * The current task will be inserted between the two tasks if his T MIN is after the T START of the previous and before the T START of the next
					 */
					
					// If the window exist, for sure the task will be inserted here
					inserted = true;
					
					// Calculating the starting point of the current reconfiguration task
					if (nonCriticTask.t_min > prev.t_end) {
						start_point = nonCriticTask.t_min;
					} else {
						start_point = prev.t_end + 1;
					}
					
				} else {
					
					// If the two task selected from the ones already scheduled has a window not good, skip to next couple
					continue;
				}
				
				// Compute T START and T END of the current reconfiguration task
				nonCriticTask.t_start = start_point;
				nonCriticTask.t_end = nonCriticTask.t_start + nonCriticTask.execution_time;
				
				// Check if the current task generate delay
				int delay_task = 0;
				if(nonCriticTask.t_end > nonCriticTask.t_max){
					
					// Propagate the delay over the subtree of the current reconfiguration task
					
					delay_task += nonCriticTask.t_end - nonCriticTask.t_max;					
					addDelayToNextTask(nonCriticTask, delay_task);
					
					for (Task t : tasksList) {
						t.updatedForDelayReconfiguration = false;
					}
					
				}
					
				// If a delay is generated, the reconfiguration tasks following have to shift on the right
				delay = nonCriticTask.t_end - next.t_start;
				
				if (delay>0) {
					
					for (int j = i+1; j < finalReconfigurationTasksLikst.size(); j++) {
						Task rt = finalReconfigurationTasksLikst.get(j);
						
						rt.t_min += delay;
						rt.t_max += delay;
						rt.t_start += delay;
						rt.t_end += delay;
						
						// Add delay to subtree of the reconfiguration task shifted
						addDelayToNextTask(rt, delay);
											
					}
					
					for (Task t : tasksList) {
						t.updatedForDelayReconfiguration = false;
					}
					
					
				}
				
				// Add the reconfiguration task to the final list
				finalReconfigurationTasksLikst.add(i+1, nonCriticTask);
				break;
			
			}

			// Check if the reconfiguration task has been inserted
			if (!inserted) {
				
				/*
				 * If not, the current reconfiguration task is put at the end of the reconfiguration task list
				 */
				
				// Take the start point of the current reconfiguration task, which is the T END of the last task
				int start_point = finalReconfigurationTasksLikst.get(finalReconfigurationTasksLikst.size()-1).t_end + 1;
				
				// Check if the T MIN is after my hypotetic start point to set the T START
				if(nonCriticTask.t_min > start_point)
					nonCriticTask.t_start = nonCriticTask.t_min;
				else
					nonCriticTask.t_start = start_point;
				
				// Compute the T END
				nonCriticTask.t_end = nonCriticTask.t_start + nonCriticTask.execution_time;
				
				// Calculate delay
				delay = nonCriticTask.t_end - nonCriticTask.outGoing.get(0).t_min + 1;
				
				// If exist delay propagate it
				if (delay>0) {
					
					addDelayToNextTask(nonCriticTask, delay);
					
					for (Task task1 : tasksList) {
						task1.updatedForDelayReconfiguration = false;
					}
					
				}
				
				// Add the current reconfiguration task to the final list of reconfiguration task
				finalReconfigurationTasksLikst.add(finalReconfigurationTasksLikst.size(), nonCriticTask);
				
			}
			
		}
		
		return finalReconfigurationTasksLikst;
		
	}
	

}
