package faster;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Parser {
	
	public int reconf_time;
	public static final float decrement_factor = 0.1f;
	public static float alpha;
	
	//CLB = 6650
	public static final int CLB = 6650;
	public static final int MIN_CLB = 50;
	public int clb_ava;
	
	//DSP = 220
	public static final int DSP = 220;
	public static final int MIN_DSP = 20;
	public int dsp_ava;
	
	//BRAM = 280
	public static final int BRAM = 280;
	public static final int MIN_BRAM = 20;
	public int bram_ava;
	
	public static final int LUT_IN_CLB = 8;
	public static final int FF_IN_CLB = 16;
	
	//weight
	public int weightClb = (100 - 93);
	public int weightDsp = (100 - 3);
	public int weightBram = (100 - 4);
	
	public static List<Task> order;
	public static List<ReconfigurationTask> reconfigurationTaskList;
	
	public static List<Task> cpu1;
	public static List<Task> cpu2;
	
	public static final String FILE = "tg_0_20.xml";
	
	public int parse_reconf () {
		
		try {
		
		File fXmlFile = new File(FILE);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
	 
		doc.getDocumentElement().normalize();
		
		NodeList x = doc.getElementsByTagName("reconfiguration_controller");
		
		return Integer.parseInt(((Element) (x.item(0).getChildNodes().item(1)) ).getAttribute("value"));
		
			
		
		} catch (Exception e) {
		e.printStackTrace();
	    }
	
		return 0;
	}
	
	public List<Task> parse() {
	
		List<Task> tasks = new ArrayList<Task>();
		
		try {
			 
			File fXmlFile = new File(FILE);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			NodeList tagPartition = doc.getElementsByTagName("partition");
			Element elementPartition = (Element) tagPartition.item(0);
			NodeList allTasks = elementPartition.getElementsByTagName("task");
			NodeList allTransfer = elementPartition.getElementsByTagName("transfer");
			
			NodeList tagImplementation = doc.getElementsByTagName("implementation");
			
			for (int temp = 0; temp < allTasks.getLength(); temp++) {
		 
				Node nNode = allTasks.item(temp);
		 
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
					
					Task task = new Task();
					task.idTask = eElement.getAttribute("id"); 
					
					for (int temp2 = 0; temp2 < tagImplementation.getLength(); temp2++) {
						
						Element implementationElement = (Element) tagImplementation.item(temp2);
						
						if (implementationElement.getElementsByTagName("task").item(0).getNodeType() == Node.ELEMENT_NODE) {
							
							boolean found = false;
							
							NodeList tagTask = implementationElement.getElementsByTagName("task");
							for (int i = 0; i < tagTask.getLength(); i++) {
								if (((Element)(tagTask.item(i))).getAttribute("id").equals(task.idTask)) {
									found = true;
								}
							}
							
							if (!found) {
								continue;
							}
							
							Element component = (Element) implementationElement.getElementsByTagName("component").item(0);
							NodeList specs = implementationElement.getElementsByTagName("specs");
                            
							for (int i = 0; i < specs.getLength(); i++) {
								
								String name = ((Element) specs.item(i)).getAttribute("name");
								int value = Integer.parseInt(((Element) specs.item(i)).getAttribute("value"));
								
								if ("HW".equals(component.getAttribute("type"))) {
									
									if ("execution_time".equals(name)) {
										task.execution_list_hw.add(value);
									} else if ("area".equals(name)) {
										task.lut_list.add(value);
									} else if ("FFs".equals(name)) {
										task.ff_list.add(value);
									} else if ("DSPs".equals(name)) {
										task.dsp_list.add(value);
									} else if ("BRAMs".equals(name)) {
										task.bram_list.add(value);
									}
									
									
	                            } else {
	                            	if ("execution_time".equals(name)) {
										task.execution_list_sw.add(value);
									}
	                            }
								
							}
							
							//Controllo se ogni lista � stata riempita.
							int size = task.execution_list_hw.size();
							if (task.lut_list.size() < size) {
								task.lut_list.add(0);
							}
							if (task.ff_list.size() < size) {
								task.ff_list.add(0);
							}
							if (task.dsp_list.size() < size) {
								task.dsp_list.add(0);
							}
							if (task.bram_list.size() < size) {
								task.bram_list.add(0);
							}
							
						}
						
					}
					
					task.extractImplementationBestRatio(weightClb, weightDsp, weightBram, FF_IN_CLB, LUT_IN_CLB);;
					
					tasks.add(task);
					
				}
			}
			
			
			for (int temp = 0; temp < allTransfer.getLength(); temp++) {
				
				Node nNode = allTransfer.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) nNode;
					
					String src = eElement.getAttribute("src");
					String tgt = eElement.getAttribute("tgt");
					
					Task srcTask = null;
					Task tgtTask = null;
					
					for (Task task : tasks) {
						if (tgt.equals(task.idTask + ".in")) {
							tgtTask = task;
						}
						
						if (src.equals(task.idTask + ".out")) {
							srcTask = task;
						}
					}
					
					srcTask.outGoing.add(tgtTask);
					tgtTask.inGoing.add(srcTask);
				}
			}
			
			
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		
		return tasks;
	
	}
	
	public List<Task> topologicalOrdering (List<Task> tasks) {
		
		int size = tasks.size();
		
		List<String> order = new ArrayList<String>();
		List<Task> orderedTask = new ArrayList<Task>();
		List<Task> taskss = parse();
		
		
		Task temp = null;
		while (order.size() < size) {
			
			for (Task t : tasks) {
				
				if (t.inGoing.size() == 0) {
					temp = t;
					break;
				}
				
			}
			
			order.add(temp.idTask);
			tasks.remove(temp);

			for (Task t : tasks) {
				
				if (t.inGoing.contains(temp)) {
					t.inGoing.remove(temp);
				}
				
			}
			
		}
		
		
		
		for (String string : order) {
			
			for (Task task : taskss) {
				if (task.idTask.equals(string)) {
					orderedTask.add(task);
					break;
				}
			}
			
		}
		
		Task sink = new Task();
		sink.idTask = "sink";
		
		for (Task task : orderedTask) {
			if (task.outGoing.isEmpty()) {
				sink.inGoing.add(task);
				task.outGoing.add(sink);
			}
		}
		
		orderedTask.add(sink);
		
		return orderedTask;
		
	}
	
	
	public void t_min_max_calculator (List<Task> tasks) {
		
		tasks.get(0).t_min = 0;
		
		for (int i = 1; i < tasks.size(); i++) {
			
			Task task = tasks.get(i);
			
			int time = 0;
			for (Task t : task.inGoing) {
				
				if (time < t.execution_time + t.t_min)
					time = t.execution_time + t.t_min;
				
			}
			
			task.t_min = time;
			
		}
		
		tasks.get(tasks.size()-1).t_max = tasks.get(tasks.size()-1).t_min;
		
		for (int i = tasks.size()-2; i >= 0; i--) {
			
			Task task = tasks.get(i);
			
			int time = task.outGoing.get(0).t_max - task.outGoing.get(0).execution_time;
			
			for (Task t : task.outGoing) {
				
				if (time > t.t_max - t.execution_time)
					time = t.t_max - t.execution_time;
				
			}
			
			task.t_max = time;
		}
	}
	
	
	public void slackCalculator (List<Task> tasks) {
		
		for (Task task : tasks) {
			task.slack = task.t_max - task.t_min - task.execution_time;
		}
		
	}
	
	public void obtainCriticalPath (Task task) {
		
		if (task.outGoing.isEmpty()) {
			return;
		}
		
		for (Task outGoing : task.outGoing) {
			if (outGoing.slack == 0) {
				outGoing.critical_path = true;
				obtainCriticalPath(outGoing);
				break;
			}
		}
		
	}
	
	public List<Task> resourcesRequiredOrdering (List<Task> tasks) {
		
		List<Task> lutOrderedList = new ArrayList<Task>();
		
		for (Task task : tasks) {
			int i = 0;
			
			if(!task.hw)
				continue;
			
			for (Task taskComodo : lutOrderedList) {
				if (task.efficiency > taskComodo.efficiency) {
					break;
				}
				i++;
			}
			lutOrderedList.add(i, task);
		}
		
		System.out.format("%-18s | %-19s | %-16s | %-13s | %-13s | %-14s | %-12s | %-18s | %-15s | %-15s | %-15s |", 
				"TASK", "SLACK", "CP", "CLB", "DSP", "BRAM", "HW", "EXE TIME", "T MIN", "T MAX", "EFF");
		System.out.println();
		System.out.format("%-18s | %-19s | %-16s | %-13s | %-13s | %-14s | %-12s | %-18s | %-15s | %-15s |  %-15s |", 
				"---------", "---------", "---------", "---------", "---------", "---------", "---------", "---------", "---------", "---------", "---------");
		System.out.println();
		
		for (Task task : lutOrderedList) {
			System.out.format("%-18s | %-19s | %-16s | %-13s | %-13s | %-14s | %-12s | %-18s | %-15s | %-15s | %-15s |", 
								task.idTask, task.slack, task.critical_path, task.clb, task.dsp, task.bram, task.hw, task.execution_time, task.t_min, task.t_max, task.efficiency);
			System.out.println();
			
		}
		
		return lutOrderedList;
		
	}
	
	public List<Area> areaDefinition (List<Task> tasks) {
		
		System.out.format("\n\n\n====== %-15s %-35s %-15s ========\n\n", "", "MAPPATURA HW DEI TASK DEL CRITICAL PATH", "");
		
		clb_ava = (int) (CLB * alpha);
		dsp_ava = (int) (DSP * alpha);
		bram_ava = (int) (BRAM * alpha);
		
		int i = 0;
		
		List<Area> areaList = new ArrayList<Area>();
		
		//Mappatura dei task critici su hardware
		for (Task task : tasks) {
			if (!task.hw || !task.critical_path) continue;
			
			boolean found = false;
			int minClb = 0;
			int minDsp = 0;
			int minBram = 0;
			Area usedArea = null;
			
			System.out.format("TASK: %-15s --> %2s", task.idTask, "");
			
			//Sarebbe da infilarlo nella arae pi� intelligente
			for (Area area : areaList) {
				if (
					area.clb >= task.clb &&
					area.dsp >= task.dsp &&
					area.bram >= task.bram &&
					noConflicts(area, task) &&
					(area.clb < minClb || area.dsp < minDsp || area.bram < minBram || !found)) {
						usedArea = area;
						minClb = area.clb;
						minDsp = area.dsp;
						minBram = area.bram;
						found = true;
				}
			}

			if (found) {
				usedArea.tasks.add(task);
				task.area_ref = usedArea;
				System.out.println("MAPPATO! task" + task.idTask + " sull'area " + usedArea.id);
			}
			
			if (ciSta(task) && !found) {
				int used_clb = 0;
				int used_dsp = 0;
				int used_bram = 0;
				
				if (task.clb != 0) {
					used_clb = (task.clb/MIN_CLB + 1)*MIN_CLB;
				}
				
				if (task.dsp != 0) {
					used_dsp = (task.dsp/MIN_DSP + 1)*MIN_DSP;
				}
				
				if (task.bram != 0) {
					used_bram = (task.bram/MIN_BRAM + 1)*MIN_BRAM;
				}
				
				System.out.println("CREO!!! una nuova area (" + i + ") per il task: " + task.idTask);
				task.area_ref = new Area(i, used_clb, used_dsp, used_bram);
				areaList.add(task.area_ref);
				task.area_ref.tasks.add(task);
				clb_ava -= used_clb;
				dsp_ava -= used_dsp;
				bram_ava -= used_bram;
				i++;
			} else if (!found){
				//Se il task non ci sta sulla FPGA lo switchamo su una implementazione SW.
				task.switchToSoftwareImplementation();
				System.out.println("WARNING! Task " + task.idTask + " diventa SW (exe time = " + task.execution_time + ")");
			}
			
		}
				
		//Mappatura in hardware dei task non critici
		System.out.format("\n\n\n====== %-15s %-35s %-15s ========\n\n", "", "MAPPATURA HW DEI TASK NON DEL CRITICAL PATH", "");
		for (Task task : tasks) {
			
			if (!task.hw || task.critical_path) continue;
			
			boolean found = false;
			int minClb = 0;
			int minDsp = 0;
			int minBram = 0;
			Area usedArea = null;
			
			System.out.format("TASK: %-15s --> %2s", task.idTask, "");
			
			if (ciSta(task)) {
				
				int used_clb = 0;
				int used_dsp = 0;
				int used_bram = 0;
				
				if (task.clb != 0) {
					used_clb = (task.clb/MIN_CLB + 1)*MIN_CLB;
				}
				
				if (task.dsp != 0) {
					used_dsp = (task.dsp/MIN_DSP + 1)*MIN_DSP;
				}
				
				if (task.bram != 0) {
					used_bram = (task.bram/MIN_BRAM + 1)*MIN_BRAM;
				}
				
				System.out.println("CREO!!! una nuova area (" + i + ") per il task: " + task.idTask);
				task.area_ref = new Area(i, used_clb, used_dsp, used_bram);
				areaList.add(task.area_ref);
				task.area_ref.tasks.add(task);
				clb_ava -= used_clb;
				dsp_ava -= used_dsp;
				bram_ava -= used_bram;
				i++;
				
			} else {
			
				//Sarebbe da infilarlo nella arae pi� intelligente
				for (Area area : areaList) {
					if (
						area.clb >= task.clb &&
						area.dsp >= task.dsp &&
						area.bram >= task.bram &&
						noConflicts(area, task) &&
						(area.clb < minClb || area.dsp < minDsp || area.bram < minBram || !found)) {
							usedArea = area;
							minClb = area.clb;
							minDsp = area.dsp;
							minBram = area.bram;
							found = true;
					}
				}
				
				if (found) {
					usedArea.tasks.add(task);
					task.area_ref = usedArea;
					System.out.println("MAPPATO! task" + task.idTask + " sull'area " + usedArea.id);
				}
				
				if (!found){
					//Se il task non ci sta sulla FPGA lo switchamo su una implementazione SW.
					task.switchToSoftwareImplementation();
					System.out.println("WARNING! Task " + task.idTask + " diventa SW (exe time = " + task.execution_time + ")");
				}
			}
			
		}
		
		System.out.println();
		
		System.out.format("\n\n====== %-15s %-13s %-15s ========\n", "", "AREE GENERATE", "");
		int quantita1 = 0;
		int quantita2 = 0;
		int quantita3 = 0;
		
		System.out.format("%-18s | %-17s | %-18s | %-13s | %-14s | %-14s | %-14s |", 
							"ID", "CLB", "CLB TILE", "DSP", " DSP TILE", "BRAM", "BRAM TILE");
		System.out.println();
		System.out.format("%-18s | %-17s | %-18s | %-13s | %-14s | %-14s | %-14s |", 
							"----------", "----------", "----------", "----------", "----------", "----------", "----------");
		System.out.println();
		
		for (Area area : areaList) {
			quantita1 += area.clb;
			quantita2 += area.dsp;
			quantita3 += area.bram;
			
			System.out.format("%-18s | %-17s | %-18s | %-13s | %-14s | %-14s | %-14s |", 
								area.id, area.clb, area.clb/MIN_CLB, area.dsp, area.dsp/MIN_DSP, area.bram, area.bram/MIN_BRAM);
			System.out.println();
			
		}
		
		System.out.println();
		System.out.println("In tutto ho: " + CLB + " CLB. E ho occupato: " + quantita1);
		System.out.println("In tutto ho: " + DSP + " DSP. E ho occupato: " + quantita2);
		System.out.println("In tutto ho: " + BRAM + " BRAM. E ho occupato: " + quantita3);
		System.out.println();
		
		//Ordinamento dei task in ogni area guardando il tmin
		for (Area area : areaList) {
			
			List<Task> comodoTaskList = area.tasks;
			area.tasks = new ArrayList<Task>();
			
			for (Task task : comodoTaskList) {
				int j = 0;
				for (Task taskComodo : area.tasks) {
					if (task.t_min < taskComodo.t_min) {
						break;
					}
					j++;
				}
				area.tasks.add(j, task);
			}
		}
		
		return areaList;
	}
	
	public boolean noConflicts(Area area, Task task) {
		for (Task taskInArea : area.tasks) {
			//Caso precedenze
			/*if (task.inGoing.contains(taskInArea) || task.outGoing.contains(taskInArea)) {
				return false;
			}*/
			
			//Caso overlap che parte dopo
			if ((task.t_min <= taskInArea.t_max && task.t_min >= taskInArea.t_min && task.t_max >= taskInArea.t_max)) {
				return false;
			}
			
			//Caso overlap che parte prima
			if ((task.t_min <= taskInArea.t_min && task.t_max >= taskInArea.t_min && task.t_max <= taskInArea.t_max)) {
				return false;
			}
			
			//Caso overlap che parte prima e finisce dopo
			if ((task.t_min <= taskInArea.t_min && task.t_max >= taskInArea.t_max)) {
				return false;
			}
			
			//Caso nester che parte dopo e finisce prima
			if ((task.t_min >= taskInArea.t_min && task.t_max <= taskInArea.t_max)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean noConflictsBalance(Area area, Task task) {
		for (Task taskInArea : area.tasks) {
			//Caso precedenze
			/*if (task.inGoing.contains(taskInArea) || task.outGoing.contains(taskInArea)) {
				return false;
			}*/
			
			//Caso overlap che parte dopo
			if ((task.t_min <= (taskInArea.t_min + taskInArea.execution_time) && task.t_min >= taskInArea.t_start && task.t_max >= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
			
			//Caso overlap che parte prima
			if ((task.t_min <= taskInArea.t_min && task.t_max >= taskInArea.t_start && task.t_max <= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
			
			//Caso overlap che parte prima e finisce dopo
			if ((task.t_min <= taskInArea.t_min && task.t_max >= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
			
			//Caso nester che parte dopo e finisce prima
			if ((task.t_min >= taskInArea.t_min && task.t_max <= (taskInArea.t_min + taskInArea.execution_time))) {
				return false;
			}
		}
		return true;
	}

	public boolean ciSta (Task task) {
		
		int used_clb = 0;
		int used_dsp = 0;
		int used_bram = 0;
		
		if (task.clb != 0) {
			used_clb = (task.clb/MIN_CLB + 1)*MIN_CLB;
		}
		
		if (task.dsp != 0) {
			used_dsp = (task.dsp/MIN_DSP + 1)*MIN_DSP;
		}
		
		if (task.bram != 0) {
			used_bram = (task.bram/MIN_BRAM + 1)*MIN_BRAM;
		}
		
		return	used_clb <= clb_ava &&	used_dsp <= dsp_ava &&	used_bram <= bram_ava;
	}
	
	
	public List<ReconfigurationTask> reconfigurationTaskCreation (List<Area> listArea) {
		
		List<ReconfigurationTask> reconfiguratioinTaskList = new ArrayList<ReconfigurationTask>();
		
		for (Area area : listArea) {
			for (int i = 0; i < area.tasks.size()-1; i++) {
				
				ReconfigurationTask reconfigTask = new ReconfigurationTask();
				reconfigTask.t_min = area.tasks.get(i).t_min + area.tasks.get(i).execution_time + 1;
				reconfigTask.t_max = area.tasks.get(i+1).t_min - 1;
				reconfigTask.inGoing.add(area.tasks.get(i));
				reconfigTask.outGoing.add(area.tasks.get(i+1));
				reconfigTask.area_ref = area;
				
//				reconfigTask.execution_time = area.clb * reconf_time / MIN_CLB * 36;
				reconfigTask.execution_time = (area.clb * reconf_time / MIN_CLB * 36) + (area.dsp * reconf_time / MIN_DSP) + (area.bram * reconf_time / MIN_BRAM);
				
				reconfigTask.idTask =
						reconfigTask.inGoing.get(0).idTask +
						" ---> " +
						reconfigTask.outGoing.get(0).idTask;
				
				if (reconfigTask.outGoing.get(0).critical_path) {
					reconfigTask.critical_path = true;
				}
				
				if (i == 0) {
					reconfigTask.prev_reconf_task = null;
				} else {
					reconfigTask.prev_reconf_task = reconfiguratioinTaskList.get(i-1);
				}				
				
				reconfiguratioinTaskList.add(reconfigTask);
				
			}
		}
		
		for (Area area : listArea) {
			System.out.format("Area: %-4s: ", area.id);
			for (Task t : area.tasks) {
				System.out.format("%-10s", t.idTask);
			}
			System.out.println();
		}
		
		System.out.format("\n\n====== %-15s %-13s %-15s ========\n\n", "", "RECONFIGURATION TASK", "");
		
		System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s |", 
				"IN TASK", "OUT TASK", "T MIN", "T MAX", "DELTA", "CP", "RECONF TIME", "CI STA?");
		System.out.println();
		System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s |", 
				"------", "------", "------", "------", "------", "------", "------", "------");
		System.out.println();
		
		for (ReconfigurationTask reconfigTask : reconfiguratioinTaskList) {
			
			System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s |", 
					reconfigTask.inGoing.get(0).idTask, reconfigTask.outGoing.get(0).idTask, reconfigTask.t_min, reconfigTask.t_max, (reconfigTask.t_max - reconfigTask.t_min), reconfigTask.critical_path, reconfigTask.execution_time, ((reconfigTask.t_max - reconfigTask.t_min) > reconfigTask.execution_time));
			System.out.println();
			
		}
		System.out.println();
		
		return reconfiguratioinTaskList;
	}
	
	//Siccome ho un solo riconfiguratore, allora creo un ordine seriale della riconfigurazione
	public List<ReconfigurationTask> reconfigurationSearilization (List <ReconfigurationTask> reconfigurationList, boolean critical) {
		
		List<ReconfigurationTask> serializedTasks = new ArrayList<ReconfigurationTask>();
		
		for (ReconfigurationTask current_rec : reconfigurationList) {
			
			if (current_rec.critical_path != critical) {
				continue;
			}
			
			int i = 0;
			for (ReconfigurationTask rec : serializedTasks) {
				if (current_rec.t_min < rec.t_min) {
					break;
				}
				i++;
			}
			serializedTasks.add(i, current_rec);
		}
		
		System.out.format("\n\n====== %-7s %-13s %-15s ========\n\n", "", "RECONFIGURATION TASK", "CRITICAL = " + critical);
		
		for (ReconfigurationTask reconfigTask : serializedTasks) {
			
			System.out.format("%10s  -->  %-10s", reconfigTask.inGoing.get(0).idTask, reconfigTask.outGoing.get(0).idTask);
			System.out.println();
			
		}
		
		System.out.println();
		
		return serializedTasks;
	}
	
	public List<ReconfigurationTask> delayCalculationCritical (List<ReconfigurationTask> reconfigurationList) {
		
		List<ReconfigurationTask> delayReconfigurationList = new ArrayList<ReconfigurationTask>();
		
		if (reconfigurationList.isEmpty()) {
			return null;
		}
		
		ReconfigurationTask rec_task = reconfigurationList.get(0);
		rec_task.t_start = rec_task.t_min;
		rec_task.t_end = rec_task.t_start + rec_task.execution_time;
		if (rec_task.t_end > rec_task.t_max) {
			
			int delay = rec_task.t_end - rec_task.t_max;
			
			addDelayToNextTask(rec_task, delay);
			
			//Ricalcolo dei tmin e tmax dei reconfiguration time
			for (ReconfigurationTask rt : reconfigurationTaskList) {
				rt.t_min = rt.inGoing.get(0).t_max;
				rt.t_max = rt.outGoing.get(0).t_min;
			}
			
			for (Task task : order) {
				task.updatedForDelayReconfiguration = false;
			}
		}
		delayReconfigurationList.add(rec_task);
		
		
		for (ReconfigurationTask reconfigurationTask : reconfigurationList) {
			
			if (delayReconfigurationList.contains(reconfigurationTask))
				continue;

			reconfigurationTask.t_min = reconfigurationTask.inGoing.get(0).t_end;
			reconfigurationTask.t_max = reconfigurationTask.outGoing.get(0).t_start;
			
			ReconfigurationTask previousRecTask = delayReconfigurationList.get(delayReconfigurationList.size()-1);
			if (reconfigurationTask.t_min > previousRecTask.t_end) {
				reconfigurationTask.t_start = reconfigurationTask.t_min;
				reconfigurationTask.t_end = reconfigurationTask.t_start + reconfigurationTask.execution_time;
			} else {
				reconfigurationTask.t_start = previousRecTask.t_end + 1;
				reconfigurationTask.t_end = reconfigurationTask.t_start + reconfigurationTask.execution_time;
			}

			if (reconfigurationTask.t_end > reconfigurationTask.t_max) {
				
				int delay = reconfigurationTask.t_end - reconfigurationTask.t_max;
				
				addDelayToNextTask(reconfigurationTask, delay);
				
				//Ricalcolo dei tmin e tmax dei reconfiguration time
				for (ReconfigurationTask rt : reconfigurationTaskList) {
					rt.t_min = rt.inGoing.get(0).t_max;
					rt.t_max = rt.outGoing.get(0).t_min;
				}
				
				for (Task task : order) {
					task.updatedForDelayReconfiguration = false;
				}
			}
			
			delayReconfigurationList.add(reconfigurationTask);
			
		}
		
		System.out.format("\n\n====== %-15s %-13s %-15s ========\n\n", "", "RECONFIGURATION DELAY CALCULATION CRITICAL", "");
		
		System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s |", 
				"IN TASK", "OUT TASK", "T MIN", "T MAX", "START", "END");
		System.out.println();
		System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s |", 
				"------", "------", "------", "------", "------", "------");
		System.out.println();
		
		for (ReconfigurationTask reconfigTask : delayReconfigurationList) {
			
			System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s |", 
					reconfigTask.inGoing.get(0).idTask, reconfigTask.outGoing.get(0).idTask, reconfigTask.t_min, reconfigTask.t_max, reconfigTask.t_start, reconfigTask.t_end);
			System.out.println();
			
		}
		
		return delayReconfigurationList;
		
	}
	
	public List<ReconfigurationTask> delayCalculationNonCritical (List<ReconfigurationTask> non_critical, List<ReconfigurationTask> critical) {
		
		List<ReconfigurationTask> critical_and_ncritical = new ArrayList<ReconfigurationTask>();
		
		if (non_critical.isEmpty()) {
			return null;
		}
		
		for (ReconfigurationTask reconfigurationTask : critical) {
			critical_and_ncritical.add(reconfigurationTask);
		}
		
		for (ReconfigurationTask n_critic : non_critical) {
			
			int delay = 0;
			boolean inserito = false;
			
			n_critic.t_min = n_critic.inGoing.get(0).t_end;
			n_critic.t_max = n_critic.outGoing.get(0).t_start;

			
			for (int i = 0; i < critical_and_ncritical.size()-1; i++) {
				
				int start_point = 0;
				
				ReconfigurationTask prev = critical_and_ncritical.get(i);
				ReconfigurationTask next = critical_and_ncritical.get(i+1);
				
				if (n_critic.t_min >= prev.t_start && n_critic.t_min < next.t_start) {
					
					inserito = true;
					
					if (n_critic.t_min > prev.t_end) {
						
						start_point = n_critic.t_min;
					} else {
						start_point = prev.t_end + 1;
					}
				} else {
					continue;
				}
				
				n_critic.t_start = start_point;
				n_critic.t_end = n_critic.t_start + n_critic.execution_time;
				
				//Ritardo dei task che m seguono
				int delay_task = 0;
				if(n_critic.t_end > n_critic.t_max){
					
					delay_task += n_critic.t_end - n_critic.t_max;					
					addDelayToNextTask(n_critic, delay_task);
					
					for (Task task1 : order) {
						task1.updatedForDelayReconfiguration = false;
					}
					
				}
					
				//shift a destra dei reconf task
				delay = n_critic.t_end - next.t_start;
				
				if (delay>0) {
					
					for (int j = i+1; j < critical_and_ncritical.size(); j++) {
						ReconfigurationTask rt = critical_and_ncritical.get(j);
						
						rt.t_min += delay;
						rt.t_max += delay;
						rt.t_start += delay;
						rt.t_end += delay;
						
						addDelayToNextTask(rt, delay);
											
					}
					
					for (Task task1 : order) {
						task1.updatedForDelayReconfiguration = false;
					}
					
					
				}
				
				critical_and_ncritical.add(i+1, n_critic);
				break;
			
			}
			
			if (!inserito) {
				
				// Se va in fondo l'overall delay non deve essere considerato.
				
				int start_point = critical_and_ncritical.get(critical_and_ncritical.size()-1).t_end + 1;
				
				// Controllo che il t_min sia dopo il fondo
				if(n_critic.t_min > start_point)
					n_critic.t_start = n_critic.t_min;
				else
					n_critic.t_start = start_point;
				
				n_critic.t_end = n_critic.t_start + n_critic.execution_time;
				
				delay = n_critic.t_end - n_critic.outGoing.get(0).t_min + 1;
				
				if (delay>0) {
					
					addDelayToNextTask(n_critic, delay);
					
					for (Task task1 : order) {
						task1.updatedForDelayReconfiguration = false;
					}
					
				}
				
				critical_and_ncritical.add(critical_and_ncritical.size(), n_critic);
				
			}
			
		}
		
		System.out.format("\n\n====== %-15s %-13s %-15s ========\n\n", "", "RECONFIGURATION DELAY CALCULATION NON CRITICAL", "");
		
		System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s |", 
				"IN TASK", "OUT TASK", "T MIN", "T MAX", "START", "END");
		System.out.println();
		System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s |", 
				"------", "------", "------", "------", "------", "------");
		System.out.println();
		
		for (ReconfigurationTask reconfigTask : non_critical) {
			
			System.out.format("%-8s --> %-8s | %-15s | %-15s | %-15s | %-15s |", 
					reconfigTask.inGoing.get(0).idTask, reconfigTask.outGoing.get(0).idTask, reconfigTask.t_min, reconfigTask.t_max, reconfigTask.t_start, reconfigTask.t_end);
			System.out.println();
			
		}
		
		return critical_and_ncritical;
		
	}

	//comprensiva di t_start e t_end
	public void addDelayToNextTask (Task startingTask, int delay) {
		
		if (startingTask.outGoing.isEmpty()) {
			return;
		}
		
		for (Task task : startingTask.outGoing) {
			
			if (startingTask.t_end < task.t_min) {
				continue;
			}
			
			int newDelay = startingTask.t_end - task.t_min + 1;
			
			
			task.t_min += newDelay;
			task.t_max += newDelay;
			task.t_start += newDelay;
			task.t_end += newDelay;
			
			if(!task.hw){
				if(task.cpu == 1)
					shiftTaskSoftware(cpu1, task.t_start - newDelay);
				else
					shiftTaskSoftware(cpu2, task.t_start - newDelay);
			}
			
			addDelayToNextTask(task, newDelay);
		}
		
		
	}
	
	public void shiftTaskSoftware(List<Task> swTask, int limit){
		
		for (int i = 0; i < swTask.size()-1; i++) {
			
			Task task = swTask.get(i);
			Task nextTask = swTask.get(i+1);
			
			if(nextTask.t_start <= limit)
				continue;
			
			if(task.t_end < nextTask.t_min)
				continue;
			
			int delay = task.t_end - nextTask.t_min + 1;
			
			nextTask.t_min += delay;
			nextTask.t_max += delay;
			nextTask.t_start += delay;
			nextTask.t_end += delay;
			
			addDelayToNextTask(task, delay);
		}
		
	}
	
	public void calculationStartEndTask() {
		
		for (Task task : order) {
			
			task.t_start = task.t_min;
			
			task.t_end = task.t_start + task.execution_time;
			
			if (task.t_end > task.t_max) {
				
				addDelayToNextTask(task, task.t_end - task.t_max);
				
				//Ricalcolo dei tmin e tmax dei reconfiguration time
				for (ReconfigurationTask rt : reconfigurationTaskList) {
					rt.t_min = rt.inGoing.get(0).t_end;
					rt.t_max = rt.outGoing.get(0).t_min;
				}
				
				for (Task task1 : order) {
					task1.updatedForDelayReconfiguration = false;
				}
			}
			
		}
		
	}
	
	

	public void scheduleTaskSoftware(List<Task> tasks) {
		
		List<Task> softwareTaskList = new ArrayList<Task>();
		cpu1 = new ArrayList<Task>();
		cpu2 = new ArrayList<Task>();
		
		
		for (Task task : tasks) {
			int i = 0;
			if (!task.hw && !"sink".equals(task.idTask)) {
				
				for (Task swTask : softwareTaskList) {
					if (task.t_min < swTask.t_min) {
						break;
					}
					i++;
				}
				
				softwareTaskList.add(i, task);
			}
		}
		
		Task lastTaskCpu1 = null;
		Task lastTaskCpu2 = null;
		int delayCpu1;
		int delayCpu2;
		
		for (Task task : softwareTaskList) {
			
			if (cpu1.isEmpty()) {
				delayCpu1 = 0;
			} else {
				lastTaskCpu1 = cpu1.get(cpu1.size()-1);
				delayCpu1 = lastTaskCpu1.t_end - task.t_min + 1;
				
				if (delayCpu1 < 0) {
					delayCpu1 = 0;
				}
			}
			
			if (cpu2.isEmpty()) {
				delayCpu2 = 0;
			} else {
				lastTaskCpu2 = cpu2.get(cpu2.size()-1);
				delayCpu2 = lastTaskCpu2.t_end - task.t_min + 1;
				
				if (delayCpu2 < 0) {
					delayCpu2 = 0;
				}
			}
			
			int delay = 0;
			
			if (delayCpu1 <= delayCpu2) {
				cpu1.add(task);
				task.cpu = 1;
				delay = delayCpu1;
			} else {
				cpu2.add(task);
				task.cpu = 2;
				delay = delayCpu2;
			}
			
			task.t_start = task.t_min + delay;
			task.t_end = task.t_start + task.execution_time;
			
			task.t_min += delay;
			task.t_max += delay;
			
			if (task.t_end > task.t_max) {
				delay += (task.t_end - task.t_max); 
			}
			
			if (delay>0) {
				
				addDelayToNextTask(task, delay);
				
				for (ReconfigurationTask rt : reconfigurationTaskList) {
					rt.t_min = rt.inGoing.get(0).t_max;
					rt.t_max = rt.outGoing.get(0).t_min;
				}
				
				for (Task task2 : order) {
					task2.updatedForDelayReconfiguration = false;
				}

			}
		}
	}
	
	public boolean checkValidity(List<Area> areas, List<ReconfigurationTask> reconfOrdered){
		
		System.out.println("\n");
		
		boolean precedence = true;
		
		//CHECK THE PRECEDENCE OVER THE TASKS
		for (Task task : order) {
			
			if (!task.inGoing.isEmpty()) {
				for (Task inTask : task.inGoing) {
					
					if(inTask.t_end > task.t_start){
						System.out.println("VALIDITY ERROR: " + task.idTask + " violate the precedence on INGOING task: " + inTask.idTask);
						precedence = false;
					}
					
				}
			}
			
			if (!task.outGoing.isEmpty()) {
				for (Task outTask : task.outGoing) {
					
					if (outTask.t_start < task.t_end) {
						System.out.println("VALIDITY ERROR: " + task.idTask + " violate the precedence on OUTGOING task: " + outTask.idTask);
						precedence = false;
					}
					
				}
			}
		}
			
		boolean reconfPrecedence = true;
		
		//CHECK THE PRECEDENCE OVER THE RECONFIGURATION TASKS
		for (ReconfigurationTask reconfigurationTask : reconfigurationTaskList) {
			
			if (reconfigurationTask.inGoing.get(0).t_end > reconfigurationTask.t_start) {
				System.out.println("VALIDITY ERROR: " + reconfigurationTask.idTask + " violate the precedence on INGOING task: " + reconfigurationTask.inGoing.get(0).idTask);
				reconfPrecedence = false;					
			}
			
			if (reconfigurationTask.outGoing.get(0).t_start < reconfigurationTask.t_end) {
				System.out.println("VALIDITY ERROR: " + reconfigurationTask.idTask + " violate the precedence on OUTGOING task: " + reconfigurationTask.inGoing.get(0).idTask);
				reconfPrecedence = false;	
			}
			
		}
		
		boolean areaPrecedence = true;
		
		//CHECK PRECEDENCE OVER TASKS ON THE SAME CPU
		for (int i = 0; i < cpu1.size()-1; i++) {
			
			Task prev = cpu1.get(i);
			Task next = cpu1.get(i+1);
			
			if (prev.t_end > next.t_start) {
				System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on CPU1 with task: " + next.idTask);
				areaPrecedence = false;
			}
			
		}
		
		for (int i = 0; i < cpu2.size()-1; i++) {
			
			Task prev = cpu2.get(i);
			Task next = cpu2.get(i+1);
			
			if (prev.t_end > next.t_start) {
				System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on CPU2 with task: " + next.idTask);
				areaPrecedence = false;
			}
			
		}	
		
		for (Area area : areas) {
			for (int i = 0; i < area.tasks.size()-1; i++) {
				
				Task prev = area.tasks.get(i);
				Task next = area.tasks.get(i+1);
				
				if (prev.t_end > next.t_start) {
					System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on area " + area.id + " with task: " + next.idTask);
					areaPrecedence = false;
				}
				
			}
		}
		
		if (reconfOrdered != null) {
			
			for (int i = 0; i < reconfOrdered.size()-1; i++) {
				
				ReconfigurationTask prev = reconfOrdered.get(i);
				ReconfigurationTask next = reconfOrdered.get(i+1);
				
				if (prev.t_end > next.t_start) {
					System.out.println("VALIDITY ERROR: " + prev.idTask + " violate the precedence on RECONFIGURATION with reconf task: " + next.idTask);
					areaPrecedence = false;
				}
				
			}
			
		}
		
		return (precedence && reconfPrecedence && areaPrecedence);
		
	}
	
	public void balanceSoftwareTask(List<Task> tasks, List<Area> areaList){
		
		List<Task> softwareTaskList = new ArrayList<Task>();
				
		for (Task task : tasks) {
			int i = 0;
			if (!task.hw && !"sink".equals(task.idTask)) {
				
				for (Task swTask : softwareTaskList) {
					if (task.t_min < swTask.t_min) {
						break;
					}
					i++;
				}
				
				softwareTaskList.add(i, task);
			}
		}
		
		for (int i = softwareTaskList.size()-1; i >= 0; i--) {
			
			Task aux = softwareTaskList.get(i);
			int estimatedReconfTimeTotal = estimateReconfTime(areaList);
			
			for (Area area : areaList) {
				
				if(aux.t_min + aux.execution_time < estimatedReconfTimeTotal)
					continue;
				
				if (area.clb >= aux.clb && area.dsp >= aux.dsp && area.bram >= aux.bram && noConflictsBalance(area, aux)) {
					
					System.out.println("POTENZIALMENTE " + aux.idTask + " SU " + area.id);
					if(aux.switchToHardwareImplementation()){
						aux.area_ref = area;
						area.tasks.add(aux);
					}					
					break;
					
				}
				
			}
			
		}
		
		//Ordinamento dei task in ogni area guardando il tmin
		for (Area area : areaList) {
			
			List<Task> comodoTaskList = area.tasks;
			area.tasks = new ArrayList<Task>();
			
			for (Task task : comodoTaskList) {
				int j = 0;
				for (Task taskComodo : area.tasks) {
					if (task.t_min < taskComodo.t_min) {
						break;
					}
					j++;
				}
				area.tasks.add(j, task);
			}
		}
		
	}
	
	public int estimateReconfTime(List<Area> aree){
	
		if(aree.isEmpty())
			return 0;
		
		int avg = 0;
		int count = 0;
		
		// calcolo il tempo di riconfigurazione medio e conto quante riconfiguraioni devo fare
		for (Area area : aree) { 
			
			int aux = (area.clb * reconf_time / MIN_CLB * 36) + (area.dsp * reconf_time / MIN_DSP) + (area.bram * reconf_time / MIN_BRAM);
			
			if (avg == 0) {
				avg = aux;
			} else {
				avg += aux;
				avg = avg / 2;
			}
			
			
			count += (area.tasks.size() - 1);
			
		}
		
		return avg * count;
		
	}
	
	@SuppressWarnings("unchecked")
	public static void main (String[] args) {
		
		/*
		 * NON MODIFICARE PER NESSUN MMOTIVO L'ORDINE DELLE FUNZIONI NEL MAIN
		 * 
		 * NEL CASO IN CUI CAMBIO IMPLEMENTAZIONE QUELO CHE DEVO FARE E' RICOMPUTARE TMIN E TMAX
		 * 
		 */		
		
		boolean result = false;
		alpha = 1;
		int iteration = 1;
		List<Area> listArea = null;
		
		/**
		 * DOVREBBE CICLARE DA QUI
		 * IN QUESTO PUNTO C'E' LA DIMINUZIONE DI RISORSE
		 */
		
		while (!result) {
			
			Parser p = new Parser();
			p.reconf_time = p.parse_reconf();
			List<Task> tasks = p.parse();
			
			System.out.println("ITERATION " + iteration + " WITH ALPHA = " + alpha);
			iteration++;
		
			order = p.topologicalOrdering(tasks);
			
			p.t_min_max_calculator(order);
			
			p.slackCalculator(order);
			
			order.get(0).critical_path = true;
			p.obtainCriticalPath(order.get(0));
			
			//GENERAZIONE DELLE AREE NECESSARIE PER LA MAPPATURA DEI TASKS
			listArea = p.areaDefinition(p.resourcesRequiredOrdering(order));
			
			//RICALCOLO IL CRITICAL PATH
			p.t_min_max_calculator(order);
			
			/**
			 * QUI C'ERA IL CALCOLO DEI RECONF TASK
			 */		
			
			//CALCOLO T_START E T_END DI TUTTI I TASK PRIMA DI SCHEDULARE I RECONFIGURATION
			//p.calculationStartEndTask();
					
					/**
					 * ??
					 * SE HO TUTTI TASK SW QUESTA COSA ME LI METTE TUTTI IN HW (SE ESISTONO AREE) NON CONTROLLA IL TEMPO DI ESECUZIONE.
					 * QUANDO SWITCHO A IMPLEMENTAZIONE HW NON CONTROLLO CHE CE NE SIA UNA NELLA LISTA
					 * ATTENZIONE CHE HO SPOSTAO LA CREAZIONE DEI RECONFIGURATION TASK DOPO QUESTE DUE FUNZIONI TRA LE COSE BLU
					 */
					
					//CHIAMATA ALLA FUNZIONE CHE GIRA I SW IN HW
					p.balanceSoftwareTask(order, listArea);
					
					p.t_min_max_calculator(order);
					
					//CALCOLO T_START E T_END DI TUTTI I TASK PRIMA DI SCHEDULARE I RECONFIGURATION
					p.calculationStartEndTask();
					
					/**
					 * ??
					 * NELLA NO CONFLICT NOUVA TENGO CONTO DEL TEMPO DI ESECUZIONE MA!! NON DELLA POSSIBILIOTA' CHE VENGANO INTRODOTTE RICONFIGURAZIONI
					 * 
					 * ORA ABBIAMO IL PROBLEMA CONTRARIO, SE AUMENTO DI MOLTO IL RECONF SPEED VANNO TUTTI IN HW MA CON RICONFIGRAZIONI CHILOMETRICHE (VEDI 50 TASK CUSTOM)
					 * 
					 * IDEA IMPLEMENTATA:
					 * 	SE TROVASSIMO UN LIMITE OLTRE AL QUALE NON SPOSTO QUELLI SW?? PER ESEMPIO UNA STIMA DEL TEMPO CHE LE RECONF CI METTONO.
					 * 	(CONTO QUANTE RECONF DEVO FARE E STIMO IL TEMPO TOTALE GUARDANDO ALL'AREA MEDIA, POTREBBE ESSERE UN COMPROESSO)
					 * 
					 */
			
			//GENERAZIONE DEI RECONFIGURATION TASKS
			reconfigurationTaskList = p.reconfigurationTaskCreation(listArea);
					
			//SCHEDULO I TASK SOFWARE
			p.scheduleTaskSoftware(order);
			
			
			//ORDINO I RECONFIGURATION TASK DEL CRITICAL PATH GUARDANDO T_MIN
			List<ReconfigurationTask> reconfigurationTaskListSerializedCritical = p.reconfigurationSearilization(reconfigurationTaskList, true);
			//SCHEDULO CALCOLANDO IL T_START E T_END DEI RECONFIGURATON TASK CRITICI
			List<ReconfigurationTask> reconfigurationTaskListDelayedCritical = p.delayCalculationCritical(reconfigurationTaskListSerializedCritical);
			
			
			
			//ORDINO I RECONFIGURATION TASK NON CRITICI GUARDANDO T_MIN
			List<ReconfigurationTask> reconfigurationTaskListSerializedNonCritical = p.reconfigurationSearilization(reconfigurationTaskList, false);
			//SCHEDULO CALCOLANDO IL T_START E T_END DEI RECONFIGURATON TASK NON CRITICI
			List<ReconfigurationTask> reconfOrdered = p.delayCalculationNonCritical(reconfigurationTaskListSerializedNonCritical, reconfigurationTaskListDelayedCritical);
		
			//VALIDITY CHECK
			System.out.println("VALIDITY RESPONSE: " + p.checkValidity(listArea, reconfOrdered));
			
	
			/////////
			
			//Region creation
			JSONObject outputJson = new JSONObject();
			
			JSONObject region_data = new JSONObject();
			
			for (Area area : listArea) {
				
				JSONObject region = new JSONObject();
				
				region.put("io", new JSONArray());
				
				JSONObject resources = new JSONObject();
				
				if (area.clb > 0) {
					resources.put("CLB", area.clb);
				}
				
				if (area.bram > 0) {
					resources.put("BRAM", area.bram);
				}
				
				if (area.dsp > 0) {
					resources.put("DSP", area.dsp);
				}
				
				region.put("resources", resources);
				
				region_data.put("rec" + area.id, region);
			}
			
			outputJson.put("regions_data", region_data);
			
			//Communications array
			outputJson.put("communications", new JSONArray());
			
			//Weight of communications object
			JSONObject obj_weights = new JSONObject();
			obj_weights.put("wirelength", 0);
			obj_weights.put("perimeter", 0);
			obj_weights.put("resources", 0);
			outputJson.put("obj_weights", obj_weights);
			
			//Cost of resources
			JSONObject res_cost = new JSONObject();
			res_cost.put("CLB", 1);
			res_cost.put("BRAM", 1);
			res_cost.put("DSP", 1);
			outputJson.put("res_cost", res_cost);
			
			outputJson.put("placement_generation_mode", "irreducible");
			
			JSONObject gurobi_params = new JSONObject();
			gurobi_params.put("MIPFocus", 1);
			outputJson.put("gurobi_params", gurobi_params);
	
			//System.out.print(outputJson);
			
	        Writer output = null;
	        String text = outputJson.toString();
	        File file = new File("output.json");
	        try {	
	        	output = new BufferedWriter(new FileWriter(file));
				output.write(text);
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
	        
	        Runtime rt = Runtime.getRuntime();
	        Process pr = null;
	        try {
				pr = rt.exec("cmd /c python floorplanner/floorplanner_cl.py output.json floorplanner/fpga/XC7Z020.json");
				pr.waitFor();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	
	        file = new File("result.json");
	       
	        text = "";
	        StringBuffer buffer = null;
	        
	        try {
	        	
	        	BufferedReader input = new BufferedReader(new FileReader(file));
	            buffer = new StringBuffer();
	            
				while ((text = input.readLine()) != null)
					buffer.append(text + "\n");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
	
	        JSONParser parser = new JSONParser();
	        JSONObject resultJson = null;
	        try {
	        	resultJson = (JSONObject) parser.parse(buffer.toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}
	        
	        result = (boolean) resultJson.get("status");
	        
	        if(result)
	        	System.out.println("FEASIBLE");
	        else
	        	alpha = alpha * (1 - decrement_factor);
     
		}
        
		////////
		
		
		/////////////////////////////////////////////////////////////GANTT CHART
		List<Task> listGantt = new ArrayList<Task>();
		for (ReconfigurationTask reconfigurationTask : reconfigurationTaskList) {
			listGantt.add(reconfigurationTask);
		}
		
		for (Task task : order) {
			if(task.idTask.equals("sink")) continue;
			listGantt.add(task);
		}
		
		Gantt2 g = new Gantt2(listGantt, listArea);
		g.setVisible(true);
//		Gantt g1 = new Gantt(listGantt, listArea);
		//g1.setVisible(true);

	}
}


