"""
@author Marco Rabozzi [marco.rabozzi@mail.polimi.it]

This is a command line version of the floorplanner.
This code requires gurobipy to be installed.

The floorplanner can be executed giving a json problem file and
a json fpga description as follows:

python floorplanner_cl.py examples/output.json fpga/XC5VLX110T.json

python floorplanner_cl.py examples/only-feasibility.json fpga/XC5VLX110T.json
python floorplanner_cl.py examples/only-feasibility.json fpga/XC7Z020.json
python floorplanner_cl.py examples/optimize-wirelength.json fpga/XC5VLX110T.json
python floorplanner_cl.py examples/optimize-resources.json fpga/XC5VLX110T.json


The script will generate the files:

result.json -> contain the result of the floorplan, if a feasible solution is found 'status' will be set to true
result.html -> generated only one a feasible solution is found, it shows a figure containing the floorplan solution

"""
import floorplanner.floorplanner as floorplanner
import sys
import json

if len(sys.argv) < 3:
	print ''
	print 'USAGE: python floorplanner_cl.py PROBLEM_FILE FPGA_FILE'
	print ''

else:
	problemFile = sys.argv[1]
	fpgaFile = sys.argv[2]


	with open(fpgaFile) as f:
		fpga = eval(f.read())
	with open(problemFile) as f:
		problem = eval(f.read())

	result = floorplanner.solve(problem, fpga, False, None)


	result_file = open("result.json","w")
	result_file.write(json.dumps(result))
	result_file.close()

	if(result['status']):

		# generate view
		with open("web_view_files/view.html") as f:
			template = f.read()		

			htmlFile = open('result.html','w')
			htmlData = template


			# generate regions data
			regionsStr = ''

			for r in result['regions']:
				region = result['regions'][r]
				recNameOrig = str(r)
				recName = recNameOrig[0:recNameOrig.find('_')]
				w = region['x2'] - region['x1'] + 1
				h = region['y2'] - region['y1'] + 1
				x = region['x1']
				y = region['y1']
				regionsStr += 'addRegion(' + str(x) + ',' + str(y) + ',' + str(w) + ',' + str(h) + ',"' + r + '",' + str(problem['regions_data'][r]['resources']) + ')\n'

			htmlData = htmlData.replace('{{{regions}}}',regionsStr);
			htmlData = htmlData.replace('{{{connections}}}','')
			htmlData = htmlData.replace('{{{fpga}}}', str(fpga))

			htmlFile.write(htmlData)
			htmlFile.close()

	