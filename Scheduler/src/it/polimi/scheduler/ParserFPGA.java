package it.polimi.scheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * TODO: javadoc
 * 
 * @author Andrea Purgato
 * @author Davide Tantillo
 *
 */

public class ParserFPGA {
	
	/**
	 * FPGA file that contain the characteristics of the FPGA chosen.
	 */
	public String fpga_file_name;
	public String fpga_model;
	
	/**
	 * Parameters that represent the FPGA chosen to run the tasks.
	 */
	public int ff_per_clb;
	public int lut_per_clb;
	public int clb_fpga;
	public int dsp_fgpa;
	public int bram_fpga;
	
	/**
	 * Parameters that represent the dimension of a single tile in the FPGA chosen to run the tasks.
	 */
	public int clb_min;
	public int dsp_min;
	public int bram_min;
	
	public ParserFPGA(String fpga_file){
		this.fpga_file_name = fpga_file;
		parse();
	}
	
	/**
	 * Function that is able to read the JSON file of the FPGA and set the FPGA parameters.
	 */
	private void parse(){
		
		File fpga_file = new File("./fpga/" + fpga_file_name);
		BufferedReader input = null;
		StringBuffer buffer = null; 
		
		JSONParser parser = new JSONParser();
        JSONObject resultJson = null;
		
		String text = null;
		
		try {
        	
        	input = new BufferedReader(new FileReader(fpga_file));
            buffer = new StringBuffer();
            
			while ((text = input.readLine()) != null)
				buffer.append(text + "\n");
			
			resultJson = (JSONObject) parser.parse(buffer.toString());
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		fpga_model = (String) resultJson.get("name");
		
		ff_per_clb = ((Long) resultJson.get("FFxCLB")).intValue();
		lut_per_clb = ((Long) resultJson.get("LUTxCLB")).intValue();
		
		JSONObject clb_object = (JSONObject) ((JSONObject) resultJson.get("resources")).get("CLB");
		JSONObject dsp_object = (JSONObject) ((JSONObject) resultJson.get("resources")).get("DSP");
		JSONObject bram_object = (JSONObject) ((JSONObject) resultJson.get("resources")).get("BRAM");
		
		clb_fpga = ((Long) clb_object.get("total")).intValue();
		clb_min = ((Long) clb_object.get("number")).intValue();
		
		dsp_fgpa = ((Long) dsp_object.get("total")).intValue();
		dsp_min = ((Long) dsp_object.get("number")).intValue();
		
		bram_fpga = ((Long) bram_object.get("total")).intValue();
		bram_min = ((Long) bram_object.get("number")).intValue();
				
	}
	

}
